﻿using UnityEngine;
using System.Collections;

public class DownloadVideoProccess : MonoBehaviour {

	//****************************************
	public DebugScript deb;//to graphical debug
	public WWW www;
	public bool IsOnDownloadProccess=false;
	IEnumerator DownloadCorountine;
	public GameObject VideoController;
	//****************************************

	// Use this for initialization
	void Start () {
		VideoController = GameObject.FindGameObjectWithTag("VideoController");
		//VideoController.GetComponent<VideoPlayController> ().RefrshButtons ();
	}
	
	// Update is called once per frame
	void Update () {
		if (IsOnDownloadProccess) {
			if (www != null) {
				if (!www.isDone) {
					this.GetComponent<SelectItem> ().LoaddingText.GetComponent<TextMesh> ().text = Mathf.CeilToInt (www.progress * 100f) + "%";
					this.GetComponent<SelectItem> ().LoaddingCircle.GetComponent<ProgressCircAutom3D> ().IsDownloadModeCircle = true;
					this.GetComponent<SelectItem> ().LoaddingCircle.GetComponent<ProgressCircAutom3D> ().PercentActual = www.progress * 100f;
				} else {
					if (SaveByteToFileProcedure ()) {
						VideoController.GetComponent<VideoPlayController> ().PassPathToLoadMPC (this.GetComponent<VideoURL> ().URL);
						VideoController.GetComponent<VideoPlayController> ().SetVideoButton (this.gameObject,
							VideoPlayController.BUTTONS_STATE.DOWNLOAD_END);
						IsOnDownloadProccess = false;
						ClearWWW ();
					} else {
						//error to save!!!
					}
				}
			}
		}
	}

	public bool CanConnectToInternet(){
		NetworkReachability NetReach;
		NetReach = Application.internetReachability;
		switch (NetReach) {
		case NetworkReachability.ReachableViaLocalAreaNetwork:
			//msg="Can connect to Internet by LAN";
			return true;
		//	break;
		case NetworkReachability.ReachableViaCarrierDataNetwork:
			//msg="Can connect to Internet by CARRIER";
			return true;
		//	break;
		case NetworkReachability.NotReachable:
			return false;
			//msg="NOT CONNECTED!!!!";
		//	break;
		default:
			return false;
			//msg="ERROR!!!!";
		//break;
		}
	}

	public void DownloadFromWeb(){//Teoric OK.....
		if (!IsOnDownloadProccess) {
			if (CanConnectToInternet()) {
				VideoController.GetComponent<VideoPlayController> ().SetVideoButton (this.gameObject,
					VideoPlayController.BUTTONS_STATE.ON_DOWNLOAD);
				//IsOnDownloadProccess = true;
				if (!VideoController.GetComponent<VideoPlayController> ().FindFilesFromURL (this.GetComponent<VideoURL> ().URL)) {
					//CancellOtherDownloads ();
					IsOnDownloadProccess = true;
					DownloadCorountine = DownloadProcedure ();
					StartCoroutine (DownloadCorountine);
				} else {
					if (SaveByteToFileProcedure ()) {
						IsOnDownloadProccess = false;
						VideoController.GetComponent<VideoPlayController> ().PassPathToLoadMPC (this.GetComponent<VideoURL> ().URL);
						VideoController.GetComponent<VideoPlayController> ().SetVideoButton (this.gameObject,
							VideoPlayController.BUTTONS_STATE.DOWNLOAD_END);
					} else {
						//Error to save!!!
					}
				}
			}
		}
	}

	private IEnumerator DownloadProcedure(){
		if (deb != null)deb.AddText ("DownloadProcedure => "+ this.GetComponent<VideoURL> ().URL);
		www = new WWW (this.GetComponent<VideoURL> ().URL);

		while (!www.isDone) {
			this.GetComponent<SelectItem> ().LoaddingText.GetComponent<TextMesh> ().text = Mathf.CeilToInt (www.progress * 100f) + "%";
			this.GetComponent<SelectItem> ().LoaddingCircle.GetComponent<ProgressCircAutom3D> ().IsDownloadModeCircle=true;
			this.GetComponent<SelectItem> ().LoaddingCircle.GetComponent<ProgressCircAutom3D> ().PercentActual = www.progress * 100f;
			yield return null;
		}
		if (SaveByteToFileProcedure ()) {
			VideoController.GetComponent<VideoPlayController>().PassPathToLoadMPC (this.GetComponent<VideoURL> ().URL);
			VideoController.GetComponent<VideoPlayController>().SetVideoButton (this.gameObject,
				VideoPlayController.BUTTONS_STATE.DOWNLOAD_END);
			IsOnDownloadProccess = false;
		} else {
			yield break;
		}
		ClearWWW ();
		yield return null;
	}


	private bool SaveByteToFileProcedure(){
		if (deb != null)
			deb.AddText ("SaveByteToFileProcedure()");
		string writePath = VideoController.GetComponent<VideoPlayController>().ConvertURLtoFileName (
			this.GetComponent<VideoURL> ().URL);
		System.IO.FileStream file = System.IO.File.Create (writePath);
		byte[] bytes = www.bytes;
		try {
			file.Write (bytes, 0, www.bytesDownloaded);
			IsOnDownloadProccess = false;
			VideoController.GetComponent<VideoPlayController>().SetVideoButton (this.gameObject,
				VideoPlayController.BUTTONS_STATE.DOWNLOAD_END);
			return true;
		} catch (System.Exception e) {
			if (deb != null)
				deb.AddText ("Error Handling Code!!!" + e.ToString ());
			IsOnDownloadProccess = false;
			this.GetComponent<SelectItem> ().LoaddingText.GetComponent<TextMesh> ().text = "Error to save!";
			return false;
		}
	}

	private void ClearWWW(){
		www.Dispose ();
		www = null;
		Resources.UnloadUnusedAssets ();
		IsOnDownloadProccess = false;
	}


	public void CancellDownload(){
		if (IsOnDownloadProccess) {
			StopCoroutine (DownloadCorountine);
			ClearWWW ();
			IsOnDownloadProccess = false;
		}
	}
}
