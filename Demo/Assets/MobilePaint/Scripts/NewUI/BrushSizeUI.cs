﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace unitycoder_MobilePaint
{

	public class BrushSizeUI : MonoBehaviour {

		public MobilePaint mobilePaint;
		private Slider slider;
		
		public PaintManager PM_;

		void Start () 
		{
			mobilePaint = PM_.mobilePaintReference;
			//mobilePaint = PaintManager.mobilePaint; // gets reference to mobilepaint through PaintManager

			if (mobilePaint==null) Debug.LogError("No MobilePaint assigned at "+transform.name, gameObject);

			slider = GetComponent<Slider>();
			if (slider==null) 
			{
				Debug.LogError("No Slider component founded",gameObject);
			}else{
			//	Debug.Log("onValueChanged");
				slider.value = mobilePaint.brushSize;
				slider.onValueChanged.AddListener((value) => { mobilePaint.SetBrushSize((int)value); });
				slider.onValueChanged.AddListener((value) => { mobilePaint.ReadCurrentCustomBrush(); });
				slider.onValueChanged.AddListener((value) => { setMobileP_(); });
				}

		}

		public void setMobileP_() {
			mobilePaint = PM_.mobilePaintReference;
		}

	}
}