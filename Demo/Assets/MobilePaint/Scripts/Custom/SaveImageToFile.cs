﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using unitycoder_MobilePaint;
using System.IO;

using System;


namespace unitycoder_MobilePaint_samples
{

	public class SaveImageToFile : MonoBehaviour
	{

		MobilePaint mobilePaint;
		public KeyCode screenshotKey = KeyCode.F12;
		public string type;//Imaginarion-HoraPintar
	
		public int ImagesSaved = 0;
		//public int ImagesSavedInDevice;
		public GameObject PopUpPanel;
		PersistentLocalize pl;
		public PaintManager _PaintM;

		void Start()
		{
			//mobilePaint = PaintManager.mobilePaint;

			pl = GameObject.FindGameObjectWithTag("Localization").GetComponent<PersistentLocalize>();
		}

		void Update()
		{
			//	if (Input.GetKeyDown(screenshotKey)) StartCoroutine(TakeScreenShot());
		}

		public void TakeIt()
		{
			mobilePaint = _PaintM.mobilePaintReference;
			StartCoroutine(TakeScreenShot());

		}

		private void Save()
		{
			pl.SaveDibujosInDevice(ScreensShotPath, ImagesSaved);
			PopUpPanel.SetActive(true);
		}

		public string ScreensShotPath;

		private IEnumerator TakeScreenShot()
		{
			ImagesSaved = PlayerPrefs.GetInt("Dibujos");
			// NOTE: this has to be called first, otherwise ReadPixels() fails inside GetScreenshot
			yield return new WaitForEndOfFrame();

			byte[] bytes = mobilePaint.GetCanvasAsTexture().EncodeToPNG(); // this works also, but only returns drawing layer
		
			DateTime today = DateTime.Now;
			string NotToday = today.ToString().Replace("/", "-");
			NotToday = NotToday.Replace("p. m.", "-");
			NotToday = NotToday.Replace(":", "-");
			NotToday = NotToday.Replace(" ", "-");


			//Debug.Log("NotToday: " + NotToday);
			ImagesSaved++;
			string filename = "screenshot-" + NotToday + ImagesSaved + ".Jpeg";
			//		string folder = Application.persistentDataPath + "/Screenshots/"; // this works also
			string folder = "";
			folder = Application.persistentDataPath + "/Screenshots/";

			string path = folder + filename;
			ScreensShotPath = path;
			if (!Directory.Exists(folder))
			{
				Directory.CreateDirectory(folder);
			}

			File.WriteAllBytes(path, bytes);
			
			Debug.Log("Screenshot saved at: " + path);
			Debug.Log("ImagesSaved ScreenShot: "   + ImagesSaved);
			PlayerPrefs.SetInt("Dibujos", ImagesSaved);
			PlayerPrefs.SetString("Dibujo_bytes" + ImagesSaved, bytes.ToString());
			Save();
		}


	}
}