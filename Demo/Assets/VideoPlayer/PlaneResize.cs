﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaneResize : MonoBehaviour
{
    
    public GameObject Plane;
    public GameObject FullScreenBG;
    public GameObject PanelTosize;
    public bool isFullScreenActive;

  //  public Camera VideoCameraRaw;

    [Header("Initial Vectors")]
    
    public Vector3 InitialMin;
    public Vector3 InitialMax;
    public Vector3 InitialPosition;

    float resWidth;
    float resHeight;
    Vector2 PlaneMin;
    Vector2 PlaneMax;
    public RawImageChangeByVideoState ri;
    public Text debug ;
    void Start()
     {

       // debug.text = "texto default";

#if UNITY_ANDROID 

        resWidth = DisplayMetricsAndroid.WidthPixels;
        resHeight = DisplayMetricsAndroid.HeightPixels;
#endif

#if UNITY_EDITOR
        resWidth = Screen.width;
        resHeight = Screen.height;
#endif
        AdjustResolution();
        ri.isWindowActive = false;
           
        InitialMin = Plane.GetComponent<RectTransform>().offsetMin;
        InitialMax = Plane.GetComponent<RectTransform>().offsetMax;
        InitialPosition = Plane.GetComponent<Transform>().localPosition;
        StartCoroutine(stayBlack());

    }
    
   public IEnumerator stayBlack() {
        yield return new WaitForSeconds(3);
        ri.isWindowActive = true;
     //   debug.text = "Screen Width: " + resWidth + ", Height: " + resHeight;
    }

    // Update is called once per frame
    void Update()
    {

#if UNITY_ANDROID 

        if (resWidth != DisplayMetricsAndroid.WidthPixels || resHeight != DisplayMetricsAndroid.HeightPixels)
        {
            //   Debug.Log("Screen.width: " + Screen.width + ", Screen.height: " + Screen.height);
            if (!isFullScreenActive)
                AdjustResolution();
            else
                AdjustResolutionInFullScreen();
            resWidth = DisplayMetricsAndroid.WidthPixels;
            resHeight = DisplayMetricsAndroid.HeightPixels;

        }
#endif

#if UNITY_EDITOR

        if (resWidth != Screen.width || resHeight != Screen.height)
        {
            //   Debug.Log("Screen.width: " + Screen.width + ", Screen.height: " + Screen.height);
            if (!isFullScreenActive)
                AdjustResolution();
            else
                AdjustResolutionInFullScreen();
            resWidth = Screen.width;
            resHeight = Screen.height;

        }
#endif


          /*Debug.Log("offsetMin: " + Plane.GetComponent<RectTransform>().offsetMin);
          Debug.Log("offsetMax: " + Plane.GetComponent<RectTransform>().offsetMax);
          Debug.Log("localPosition: " + Plane.GetComponent<Transform>().localPosition);
          */
      
    }


    public void  Adjustres() {
       
        if (!isFullScreenActive)
            AdjustResolution();
        else
            AdjustResolutionInFullScreen();

    }
    public void AdjustResolution()
    {
        FullScreenBG.SetActive(false);
        Plane.GetComponent<Transform>().eulerAngles = new Vector3(0, 0, 0);
        
        isFullScreenActive = false;
        Plane.GetComponent<RectTransform>().offsetMin = InitialMin;
        Plane.GetComponent<RectTransform>().offsetMax = InitialMax;
        Plane.GetComponent<Transform>().localPosition = InitialPosition;


    }
    Vector2 ScreenSizeDevice;
    private void AdjustResolutionInFullScreen()
    {

        FullScreenBG.SetActive(true);
#if UNITY_ANDROID
        ScreenSizeDevice.x = DisplayMetricsAndroid.WidthPixels;
        ScreenSizeDevice.y = DisplayMetricsAndroid.HeightPixels;
        debug.text = "Entró en Android";
#endif
#if UNITY_EDITOR
        ScreenSizeDevice.x = Screen.width;
        ScreenSizeDevice.y = Screen.height;
#endif


        Plane.GetComponent<Transform>().eulerAngles = new Vector3(0, 0, 90);
      //  debug.text = "///ScreenSizeDevice.x: " + ScreenSizeDevice.x + ", ScreenSizeDevice.y: " + ScreenSizeDevice.y;

        if (ScreenSizeDevice.x == 320 && ScreenSizeDevice.y == 480)
        { //*
            /*Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-64.5f, -99.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(64.5f, 21.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(0.0f, -16.1f, 0.0f);*/
        }
        if (ScreenSizeDevice.x == 480 && ScreenSizeDevice.y == 854)//*
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-160.5f, -131.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(158.5f, -5.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(-1.0f, -27.3f, 0.0f);
        }
        if (ScreenSizeDevice.x == 600 && ScreenSizeDevice.y == 1024)//*
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-179.0f, -170.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(179.0f, 4.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(0.0f, -34.2f, 0.0f);
        }
        if (ScreenSizeDevice.x == 800 && ScreenSizeDevice.y == 1280)//*
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-198.5f, -235.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(196.5f, 33.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(-1.0f, -40.0f, 0.0f);
        }


        if (ScreenSizeDevice.x == 1080 && ScreenSizeDevice.y == 2160)//*
        {
            //  Debug.Log("FullResolution 1080*2160");

            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-455.5f, -268.5f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(467.5f, -89.5f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(6, -85.1f, 0);

        }

        /* Resolution: 480*800 */
        if (ScreenSizeDevice.x == 480 && ScreenSizeDevice.y == 800)//*
        {
            //    Debug.Log("FullResolution 480*800");

            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-135.5f, -138.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(133.5f, 10.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(-1.0f, -25.9f, 0.0f);

        }

        /* Resolution: 720*1280 - Plane: 80x51 */
        if (ScreenSizeDevice.x == 720 && ScreenSizeDevice.y == 1280)//*
        {
            //   Debug.Log("FullResolution 720*1280");

            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-238.5f, -197.5f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(234.5f, -10.5f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(-2.0f, -43.0f, 0.0f);

        }

        /* Resolution: 1080*1920 - Plane: 80x51 */
        if (ScreenSizeDevice.x == 1080 && ScreenSizeDevice.y == 1920)//*
        {
            //    Debug.Log("FullResolution 1080*1920");

            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-353.5f, -297.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(353.5f, -23.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(0.0f, -68.5f, 0.0f);

        }

        /* Resolution: 1440*2560 - Plane: 80x51 */
        if (ScreenSizeDevice.x == 1440 && ScreenSizeDevice.y == 2560)//*
        {
            //   Debug.Log("FullResolution 1440*2560");

            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-475.0f, -394.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(471.0f, -20.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(-2.0f, -85.1f, 0.0f);

        }

        /* Resolution: 1440*2960 - Plane: 70x44 */
        if (ScreenSizeDevice.x == 1440 && ScreenSizeDevice.y == 2960)//*
        {
            //    Debug.Log("FullResolution 1440*2960");

            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-659.0f, -342.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(659.0f, -138.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(0.0f, -99.0f, 0.0f);

        }


        /* Resolution: 454*807 - Plane: 80x51*/
        if (ScreenSizeDevice.x == 454 && ScreenSizeDevice.y == 807)
        {
            //    Debug.Log("FullResolution 454*807");

            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-150.5f, -124.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(150.5f, -8.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(0.0f, -27.6f, 0.0f);

        }

        /* Resolution:454*908 - Plane: 71x45 */
        if (ScreenSizeDevice.x == 454 && ScreenSizeDevice.y == 908)
        {
            //   Debug.Log("FullResolution 454*908");

            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-196.5f, -109.5f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(198.5f, -36.5f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(1.0f, -29.7f, 0.0f);

        }


        if (ScreenSizeDevice.x == 1080 && ScreenSizeDevice.y == 2248)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-512.0f, -257.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(498.0f, -115.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(-7.0f, -78.9f, 0.0f);
        }

        if (ScreenSizeDevice.x == 1080 && ScreenSizeDevice.y == 2280)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-509.0f, -258.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(515.0f, -126.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(3.0f, -83.4f, 0.0f);
        }
        if (ScreenSizeDevice.x == 1125 && ScreenSizeDevice.y == 2036)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-383.0f, -320.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(373.0f, -34.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(-5.0f, -80.0f, 0.0f);

        }
        if (ScreenSizeDevice.x == 1200 && ScreenSizeDevice.y == 1920)
        {

            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-280.5f, -372.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(286.5f, 46.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(3.0f, -71.5f, 0.0f);

        }

        if (ScreenSizeDevice.x == 1242 && ScreenSizeDevice.y == 2688)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-634.5f, -285.5f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(620.5f, -162.5f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(-7.0f, -96.0f, 0.0f);
        }


        if (ScreenSizeDevice.x == 1334 && ScreenSizeDevice.y == 750)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-244.0f, -214.5f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(238.0f, -15.5f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(-3.0f, -51.5f, 0.0f);
        }

        if (ScreenSizeDevice.x == 1440 && ScreenSizeDevice.y == 2880)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-619.5f, -357.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(619.5f, -129.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(0.0f, -105.8f, 0.0f);
        }

        if (ScreenSizeDevice.x == 1536 && ScreenSizeDevice.y == 2048)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-177.5f, -524.5f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(187.5f, 160.5f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(5.0f, -84.4f, 0.0f);
        }
        //***------>
        if (ScreenSizeDevice.x == 1125 && ScreenSizeDevice.y == 2046)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-386.5f, -315.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(388.5f, -31.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(1.0f, -75.5f, 0.0f);
        }

        if (ScreenSizeDevice.x == 1440 && ScreenSizeDevice.y == 2160)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-283.5f, -461.5f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(277.5f, 99.5f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(-3.0f, -78.1f, 0.0f);
        }
        if (ScreenSizeDevice.x == 1600 && ScreenSizeDevice.y == 2560)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-391.0f, -479.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(399.0f, 45.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(4.0f, -95.1f, 0.0f);
        }
        if (ScreenSizeDevice.x == 1700 && ScreenSizeDevice.y == 2560)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-329.0f, -532.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(337.0f, 98.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(4.0f, -95.1f, 0.0f);
        }
        if (ScreenSizeDevice.x == 1824 && ScreenSizeDevice.y == 2736)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-356.0f, -576.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(346.0f, 108.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(-5.0f, -103.7f, 0.0f);
        }
        if (ScreenSizeDevice.x == 2000 && ScreenSizeDevice.y == 3000)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-378.0f, -634.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(386.0f, 104.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(4.0f, -122.1f, 0.0f);
        }
        if (ScreenSizeDevice.x == 1080 && ScreenSizeDevice.y == 2340)
        {
             Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-549.0f, -245.0f);
             Plane.GetComponent<RectTransform>().offsetMax = new Vector2 (541.0f, -137.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(-4.0f, -78.7f, 0.0f);
             
        }
        if (ScreenSizeDevice.x == 720 && ScreenSizeDevice.y == 1440)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-311.0f, -186.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(313.0f, -64.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(1.0f, -55.9f, 0.0f);

        }

        if (ScreenSizeDevice.x == 720 && ScreenSizeDevice.y == 1339)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-262.5f, -190.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(268.5f, -34.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(3.0f, -47.7f, 0.0f);
            
        }
        if (ScreenSizeDevice.x == 1080 && ScreenSizeDevice.y == 2009)
        {
            Plane.GetComponent<RectTransform>().offsetMin = new Vector2(-394.5f, -292.0f);
            Plane.GetComponent<RectTransform>().offsetMax = new Vector2(394.5f, -52.0f);
            Plane.GetComponent<Transform>().localPosition = new Vector3(0.0f, -75.6f, 0.0f);

        }
    }
}


