﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;


public class WindowVideoPlayer : MonoBehaviour
{
    public bool iSFavoriteScene = false;
    [Header("Panel UI")]
    public Button CloseButton;
    public Button FavoriteButton;
    public GameObject Window;
    public GameObject videoRep;
    public GameObject DownloadPanel;
    public Text DebugText;
    public Text ProgressText;
    [Header("Controls UI")]
    public Button Play;
    public Button Pause;
    public Button FullScreen;
    public Text VideoTime;
    public Color SelectedColor;
    public Color NotSelectedColor;


    [Header("")]
    public PlaneResize PlaneVideo;
   // public MeshRenderer VideoPlane;
    //public VideoPlayer VideoPlayer;
    public AudioCuentosTiles SelectedTile;
    public string SelectedTileName;
    public PersistentLocalize persistent;
    public Favorites favorite;
    public enum Favorites { 
        FavCuentos,
        FavLeer,
        FavDibujos
    }


    void Start()
    {
        CloseButton.onClick.AddListener(CloseWindow);
        persistent = GameObject.FindGameObjectWithTag("Localization").GetComponent<PersistentLocalize>();
        if (FavoriteButton != null)
        {
            FavoriteButton.onClick.AddListener(FavoritesBTN);
        }
        Window.SetActive(false);

    }

    public void PlayVideoBTN()
    {
        videoRep.GetComponent<MediaPlayerCtrl_Mod>().Play();

        Play.GetComponent<Image>().color = SelectedColor;
        Pause.GetComponent<Image>().color = NotSelectedColor;
    }

    public void PauseVideoBTN()
    {
       videoRep.GetComponent<MediaPlayerCtrl_Mod>().Pause();
        Pause.GetComponent<Image>().color = SelectedColor;
        Play.GetComponent<Image>().color = NotSelectedColor;
    }
    public void StopVideo()
    {
        videoRep.GetComponent<MediaPlayerCtrl_Mod>().Stop();
        videoRep.GetComponent<MediaPlayerCtrl_Mod>().UnLoad();
    }

    public void FullScreenVideoBTN()
    {
        PlaneVideo.GetComponent<PlaneResize>().isFullScreenActive = !PlaneVideo.GetComponent<PlaneResize>().isFullScreenActive;
        
    }
    public void FavoritesBTN()
    {
        PauseVideoBTN();
        DownloadPanel.SetActive(true);
        persistent.AddFavoriteElement(favorite.ToString(), SelectedTile);
       
    }

    public void CloseDownloadPanel()
    {
        DownloadPanel.SetActive(false);
        PlayVideoBTN();
    }
    public void CloseWindow() {
       
        videoRep.GetComponent<MediaPlayerCtrl_Mod>().Stop();
        videoRep.GetComponent<MediaPlayerCtrl_Mod>().UnLoad();
        videoRep.GetComponent<MediaPlayerCtrl_Mod>().m_strFileName = "";
        PlaneVideo.GetComponent<PlaneResize>().isFullScreenActive = false;
        PlaneVideo.GetComponent<PlaneResize>().AdjustResolution();
        Window.gameObject.SetActive(false);
        ProgressText.gameObject.SetActive(false);
    }

public void OpenWindow(string url)
    {
        //Debug.Log("OpenWindow");
        Window.gameObject.SetActive(true);      
        videoRep.GetComponent<MediaPlayerCtrl_Mod>().m_strFileName = url;
        videoRep.GetComponent<MediaPlayerCtrl_Mod>().Load(url);
        //videoRep.GetComponent<MediaPlayerCtrl_Mod>().readyState();
        videoRep.GetComponent<MediaPlayerCtrl_Mod>().Play();
       
    
    

    }

    public void OpenFavWindow(string url)
    {
        Debug.Log("OpenWindow");
        Window.gameObject.SetActive(true);
        Debug.Log("OpenWindow/url: " + url);
        DebugText.text = url;
        videoRep.GetComponent<MediaPlayerCtrl_Mod>().m_strFileName = url;
        videoRep.GetComponent<MediaPlayerCtrl_Mod>().Load(url);        
        videoRep.GetComponent<MediaPlayerCtrl_Mod>().Play();
       

    }

    int Video_time;
    private void Update()
    {/*
        if (Video_time != null)
        {
            Video_time = GetComponent<MediaPlayerCtrl_Mod>().GetDuration();
            VideoTime.text = Video_time.ToString();
            //TimeSpan.FromSeconds(Video_time).ToString();
        }*/
    }


    public void DownloadVideo()
    {
        PauseVideoBTN();
        videoRep.GetComponent<MediaPlayerCtrl_Mod>()._DownloadStreamingVideoAndLoad(videoRep.GetComponent<MediaPlayerCtrl_Mod>().m_strFileName, SelectedTile.ContentId.ToString(), SelectedTile);
        persistent.DownloadAndSaveVideo(SelectedTile.urlToOpen, SelectedTile.ContentId.ToString());
        DownloadPanel.SetActive(false);
    }


}

