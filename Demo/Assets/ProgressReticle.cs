﻿using UnityEngine;
using System.Collections;

public class ProgressReticle : MonoBehaviour {

	public GameObject Obj00;
	public GameObject Obj01;
	public GameObject Obj02;
	public GameObject Obj03;
	public GameObject Obj04;
	public GameObject Obj05;
	public GameObject Obj06;
	public GameObject Obj07;
	public GameObject Obj08;
	public GameObject Obj09;
	public GameObject Obj10;
	public GameObject Obj11;
	public GameObject Obj12;
	public GameObject Obj13;
	public GameObject Obj14;
	public GameObject Obj15;
	public float Progress;
	public float Time0;
	public float TimeElapsedExample=0.25f;
	public float ScaleBig=2f;//0.025f
	public float ScaleNormal=2f;//0.02f
	public float ScaleSmall=2f;//0.015f

	VideoPlayController script;
	public bool LoadingRet;

	// Use this for initialization
	void Start () {
		Time0 = Time.time;
	

	}

	public void IncreaseProgress(){
		if (Progress + 1 <= 16) {
			Progress+=1.3f;
		} else {
			Progress=0;

		}
	}

	public void DecreaseProgress(){
		if (Progress - 1 >= 0) {
			Progress--;
		} else {
			Progress=16;
		}
	}

	public void CheckObjct(GameObject obj, int ProgChk){
		if (Progress > ProgChk) {
			obj.SetActive (true);
			if (Progress == ProgChk+1) {
				obj.transform.localScale= new Vector3(ScaleBig,ScaleBig,ScaleBig);
			}else{
				if (Progress == ProgChk+2) {
					obj.transform.localScale= new Vector3(ScaleNormal,ScaleNormal,ScaleNormal);
				}else{
					obj.transform.localScale= new Vector3(ScaleSmall,ScaleSmall,ScaleSmall);
				}
			}
		} else {
			obj.SetActive (false);
		}
	}

	public void RefreshCircle(){
		CheckObjct (Obj00, 0);
		CheckObjct (Obj01, 1);
		CheckObjct (Obj02, 2);
		CheckObjct (Obj03, 3);
		CheckObjct (Obj04, 4);
		CheckObjct (Obj05, 5);
		CheckObjct (Obj06, 6);
		CheckObjct (Obj07, 7);
		CheckObjct (Obj08, 8);
		CheckObjct (Obj09, 9);
		CheckObjct (Obj10, 10);
		CheckObjct (Obj11, 11);
		CheckObjct (Obj12, 12);
		CheckObjct (Obj13, 13);
		CheckObjct (Obj14, 14);
		CheckObjct (Obj15, 15);

	}

	void Example(){
		if (Time.time - Time0 > TimeElapsedExample) {
			IncreaseProgress ();
			RefreshCircle ();
			Time0 = Time.time;

		}
	}

	// Update is called once per frame
	void Update () {
		//		Loading = script.LoaddingEnd;
		//		if ( Loading == true) {
		//			DImage.SetActive(false);
		//		}
		if (LoadingRet == true) {
			Example ();

			if (Progress >= 16) {

				Debug.Log("llego a 3!!");
				LoadingRet = false;
				Progress = 0;


			}
		} else {
			Progress = 0;
			Obj00.SetActive(false);
			Obj01.SetActive(false);
			Obj02.SetActive(false);
			Obj03.SetActive(false);
			Obj04.SetActive(false);
			Obj05.SetActive(false);
			Obj06.SetActive(false);
			Obj07.SetActive(false);
			Obj08.SetActive(false);
			Obj09.SetActive(false);
			Obj10.SetActive(false);
			Obj11.SetActive(false);
			Obj12.SetActive(false);
			Obj13.SetActive(false);
			Obj14.SetActive(false);
			Obj15.SetActive(false);
		}
	}
}
