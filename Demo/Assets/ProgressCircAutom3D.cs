﻿using UnityEngine;
using System.Collections;

public class ProgressCircAutom3D : MonoBehaviour {

	public GameObject[] Obj = new GameObject[16];
	public int Progress;
	public float Time0;
	public float TimeElapsedExample=0.25f;
	public float ScaleBig=2f;//0.025f
	public float ScaleNormal=2f;//0.02f
	public float ScaleSmall=2f;//0.015f
	//public GameObject DImage;
	VideoPlayController script;
	public bool Loading;
	//public float Parts=16f;
	public float PercentActual;
	//public float difffloat;
	//public float difffloatneg;
	//public float Angle;
	//public float Angleneg;
	public bool IsDownloadModeCircle=true;



	// Use this for initialization
	void Start () {
		Time0 = Time.time;
		//DImage.SetActive (true);

	}
	
	public void IncreaseProgress(){
		if (Progress + 1 <= Obj.Length) {
			Progress++;
		} else {
			Progress=0;
		}
	}
	
	public void DecreaseProgress(){
		if (Progress - 1 >= 0) {
			Progress--;
		} else {
			Progress=Obj.Length;
		}
	}
	
	public void CheckObjct(GameObject obj, int ProgChk){
		if (Progress > ProgChk) {
			obj.SetActive (true);
			if (Progress == ProgChk+1) {
				obj.transform.localScale= new Vector3(ScaleBig,ScaleBig,ScaleBig);
			}else{
				if (Progress == ProgChk+2) {
					obj.transform.localScale= new Vector3(ScaleNormal,ScaleNormal,ScaleNormal);
				}else{
					obj.transform.localScale= new Vector3(ScaleSmall,ScaleSmall,ScaleSmall);
				}
			}
		} else {
			obj.SetActive (false);
		}
	}
	
	public void RefreshCircle(){
		for (int i = 0; i < Obj.Length; i++)
		{
			CheckObjct (Obj [i], i);
		}
	}
	
	public void Autoplay(){
		if (Time.time - Time0 > TimeElapsedExample) {
			IncreaseProgress ();
			RefreshCircle ();
			Time0 = Time.time;
		}
	}



	public void ShowProgressPercent (float percent){
		int RealProgress = Mathf.CeilToInt (percent*(Obj.Length/100f));
		//difffloat = (100f/Obj.Length)-((RealProgress*(100f/Obj.Length))-percent);
		//difffloatneg = percent-(RealProgress*(100f/Obj.Length));

		if (RealProgress <= 1) {
			Progress = 1;
		} else {
			Progress = RealProgress-1;
		}
		//=(((100/(100/16))*K11)/100)*(360/16)
		//Angle=(((100f/(100f/Obj.Length))*difffloat )/100f)*(360f/Obj.Length);
		RefreshCircle ();
	}


	// Update is called once per frame
	void Update () {
//		Loading = script.LoaddingEnd;
//		if ( Loading == true) {
//			DImage.SetActive(false);
//		}

		if (IsDownloadModeCircle) {
			ShowProgressPercent (PercentActual);
		} else {
			Autoplay ();
		}
		
		
	}
}
