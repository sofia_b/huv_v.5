﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SizeCameraTo : MonoBehaviour {

	public Camera Cam;
	Vector3 localPosition;
	Vector3 localEulerAngles;
	Vector3 localScale;
	// Use this for initialization
	void Start () {
		/*
		localPosition = new Vector3(0, 0, 0.5f / Mathf.Tan(Cam.fieldOfView / 2 * Mathf.Deg2Rad));
		localEulerAngles = new Vector3(0, -90, 0); // so that the unity plane stands upright in XY
		localScale = new Vector3(1, 1, 1);

		this.transform.localPosition = localPosition;
		//this.transform.localEulerAngles = localEulerAngles;
		this.transform.localScale = localScale;*/
		//GetElementScreenRect(this.transform);
		float scl = 2f / Screen.height * (this.localScale.z - Cam.transform.localScale.z) * Mathf.Tan(Cam.fieldOfView / 2f * Mathf.PI / 180);
		this.localScale = new Vector3 (0.1f , 1000 , scl);

	}
	public Rect GetElementScreenRect(Transform go)
	{
		Rect result = new Rect();
		result.width = Screen.height * 10 * go.localScale.z;
		result.height = Screen.height * 10 * go.localScale.x;
		result.x = 0.5f * Screen.width + go.localPosition.x * Screen.height - result.width * 0.5f;
		result.y = Screen.height - Screen.height * (0.5f + go.localPosition.z) - result.height * 0.5f; //a slight difference...

		return result;
	}
	// Update is called once per frame
	void Update () {
		
	}
}
