﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.Video;
//using Lean.Localization;

public class LocalizedSplash : MonoBehaviour
{
    public GameObject pl;

    public int SplashNumber = 1;

    public string Current;
    void Start()
    {
        pl = GameObject.FindGameObjectWithTag("Localization");
        Current = pl.GetComponent<PersistentLocalize>().CurrentLanguageName;
        Debug.Log("Current: " + Current);
        /* 
         Current = PlayerPrefs.GetString("Language");
         if (Current == "")
         {
             Current = "Spanish";
         }

         Debug.Log("Current: " + Current);*/

        if (SplashNumber == 1)
        splash1(Current);

        if (SplashNumber == 2 )
            splash2(Current);

       
    }

    private void splash1(string Current) {
        if (Current == "Spanish")
        {
            //#if UNITY_EDITOR || UNITY_ANDROID
            this.GetComponent<MediaPlayerCtrl_Mod>().m_strFileName = "SPLASHSCREEN2.mp4";
            
          //  Debug.Log("Spanish!!  " + this.GetComponent<VideoPlayer>().url);
          //#endif
        }
        if (Current == "English")
        {
              Debug.Log("English!!");
            //#if UNITY_EDITOR || UNITY_ANDROID
            this.GetComponent<MediaPlayerCtrl_Mod>().m_strFileName = "SPLASHSCREENingles.mp4";
            //#endif
        }
        if (Current == "Portuguese")
        {
             Debug.Log("Portuguese!!");
            //#if UNITY_EDITOR || UNITY_ANDROID
            this.GetComponent<MediaPlayerCtrl_Mod>().m_strFileName = "SPLASHSCREENPORTUGUES.mp4";
            //#endif
        }

        this.GetComponent<MediaPlayerCtrl_Mod>().Play();
    }

    private void splash2(string Current)
    {
        if (Current == "Spanish")
        {
            //#if UNITY_EDITOR || UNITY_ANDROID
            this.GetComponent<MediaPlayerCtrl_Mod>().m_strFileName = "TEASERVERTICALESP.mp4";
            //  Debug.Log("Spanish!!  " + this.GetComponent<VideoPlayer>().url);
            //#endif
        }
        if (Current == "English")
        {
            Debug.Log("English!!");
            //#if UNITY_EDITOR || UNITY_ANDROID
            this.GetComponent<MediaPlayerCtrl_Mod>().m_strFileName = "TEASERVERTICALING.mp4";
            //#endif
        }
        if (Current == "Portuguese")
        {
            Debug.Log("Portuguese!!");
            //#if UNITY_EDITOR || UNITY_ANDROID
            this.GetComponent<MediaPlayerCtrl_Mod>().m_strFileName = "TEASERVERTICALport.mp4";
            //#endif
        }

        this.GetComponent<MediaPlayerCtrl_Mod>().Play();
    }

    
    

    
}
