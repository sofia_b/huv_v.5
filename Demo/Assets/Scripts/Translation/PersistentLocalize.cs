﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Networking;
//using SimpleJSON;



public class PersistentLocalize : MonoBehaviour
{
    public string CurrentLanguageName;
    public Sprite CurrentLanguageImage;
    public int ActualLanguageInt;
    [Header("Favoritos")]
  //  public List<AudioCuentosTiles> AudioCuentosFav = new List<AudioCuentosTiles>();
    public List<AudioCuentosTiles> AprendiendoLeerFav = new List<AudioCuentosTiles>();
    public List<string> DibujosFav = new List<string>();
    public GameObject FavContainer;
    [Header("Hora de pintar")]
    public string CharacterSelected;
    [Header("Scenes")]
    string CurrentScene = "SCN_MainMenu";
    [Header("Trivias")]
    public string TriviaSelected;
    [Header("PersistentLang")]
    InitialPersistent InitialP;


    void Awake()
    {
        InitialP = GameObject.FindGameObjectWithTag("InitialPersistent").GetComponent<InitialPersistent>();

        DontDestroyOnLoad(transform.gameObject);
        if (InitialP.isFirstTime == true) {
            CurrentLanguageName = InitialP.SettedLanguage;
            PlayerPrefs.SetString("IsFirstTime", "false");
        }
        else
        {
            if (PlayerPrefs.GetString("Language") != null)
            {
                GetPreferences();
            }
        }

        if (PlayerPrefs.GetString("Sound") != null)
        {
           string b = PlayerPrefs.GetString("Sound");
            if (b == "true")
                SoundsON = true;
            if (b == "false")
                SoundsON = false;
        }

    }


    void Update()
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != CurrentScene)
        {
            //Debug.Log("CurrentScene: " + CurrentScene);
            SetSound();

            CurrentScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;

        }
    }
    public void SavePreferences()
    {
        PlayerPrefs.SetString("Language", CurrentLanguageName);

    }
    public void GetPreferences()
    {
        CurrentLanguageName = PlayerPrefs.GetString("Language");

    }
    public void AddFavoriteElement(string tag, AudioCuentosTiles tile)
    {
        // Debug.Log("tag: " + tag);
        bool isInTheList = false;

        // AudioCuentosTiles newcuento = new AudioCuentosTiles();
        if (tag == "FavCuentos")
        {
            if (isInTheList == false)
            {
                //save in device here 
                //SaveInDevice();
                GameObject newGo = Instantiate(tile.gameObject);
                newGo.transform.SetParent(FavContainer.transform);
                AudioCuentosTiles NewCuento = newGo.GetComponent<AudioCuentosTiles>();
               // AudioCuentosFav.Add(newGo.GetComponent<AudioCuentosTiles>());
                SaveInDevice(tile.ContentId, tile.Title, tile.IMGBytes, NewCuento, tile.CategoryName);
            }


        }

        if (tag == "FavLeer")
        {
            if (AprendiendoLeerFav.Count != 0)
            {
                foreach (AudioCuentosTiles ac in AprendiendoLeerFav)
                {
                    if (ac.Title == tile.Title)
                    {
                        isInTheList = true;
                    }
                }
            }
            if (isInTheList == false)
            {
                GameObject newGo = Instantiate(tile.gameObject);
                newGo.transform.SetParent(FavContainer.transform);
                AudioCuentosTiles newLeer =  newGo.GetComponent<AudioCuentosTiles>();
               // AprendiendoLeerFav.Add(newGo.GetComponent<AudioCuentosTiles>());

                SaveInDevice(tile.ContentId, tile.Title, tile.IMGBytes, newLeer, tile.CategoryName);
            }
        }


    }

    public void SaveInDevice(int id, string name, byte[] imgbyte, AudioCuentosTiles NewCuento, string _type)
    {
        string pathStart = "";
        //1)Create Folder of element with id
        if (_type == "Audiocuentos" || _type == "Audiocontos" || _type == "Audiotales")
            pathStart = Application.persistentDataPath + "CuentosAudios" + id;
        if (_type == "Aprendiendo a Leer" || _type == "Aprendendo a Ler" || _type == "Time to Read")
            pathStart = Application.persistentDataPath + "CuentosLeer" + id;
        Debug.Log("_type: " + _type);
        Debug.Log("NewFolder To Create: " + pathStart);
        Debug.Log("pathStart: " + Directory.Exists(pathStart));

        if (Directory.Exists(pathStart) == false)
        {
            Debug.Log("pathStart not exist" );
            System.IO.Directory.CreateDirectory(pathStart);
            Debug.Log("NewFolder: " + pathStart);
        }
        //2)Download Image to device.
        saveImage(pathStart, imgbyte, id.ToString(), NewCuento, _type, name);

    }


    void saveImage(string path, byte[] imageBytes, string id, AudioCuentosTiles _NewCuento, string type, string title)
    {
        string p;
#if UNITY_EDITOR
        p = path + "\\" + id + ".jpeg";

        File.WriteAllBytes(path + "\\" + id + ".jpeg", imageBytes);
           Debug.Log("Saved Data to: " + p);

#endif
#if UNITY_ANDROID
        p = path + "/" + id + ".jpeg";

        File.WriteAllBytes(path + "/" + id + ".jpeg", imageBytes);

#endif

#if UNITY_IOS
        //Application.persistentDataPath +/30.wtc"; //iOS
        p = path + "/" + id + ".jpeg";
        File.WriteAllBytes(path + "/" + id + ".jpeg", imageBytes);
#endif
        if (type == "Audiocuentos" || type == "Audiocontos" || type == "Audiotales")            
            SetAudioCuentos(_NewCuento, p, type, title);
        if (type == "Aprendiendo a Leer" || type == "Aprendendo a Ler" || type == "Time to Read")
            SetLeerCuentos(_NewCuento, p, type, title);
    }


   
    public int AuxElements = 0;
   
    void SetAudioCuentos(AudioCuentosTiles _NewCuento, string ImageSaved, string type, string title)
    {
        AuxElements = PlayerPrefs.GetInt("AudioCuentosGuardados");//3
        Debug.Log("AudioCuentosGuardados 0: " + AuxElements);

        //---
            Debug.Log("_NewCuento.ContentId: " + _NewCuento.ContentId);
            int id = _NewCuento.ContentId;
        
            PlayerPrefs.SetInt("AudioID" + AuxElements, id);       

            PlayerPrefs.SetString("AudioImage" + id, ImageSaved);
            Debug.Log("Set Image" + ImageSaved);
            PlayerPrefs.SetString("AudioType" + id, _NewCuento.CategoryName);
           
            PlayerPrefs.SetString("AudioTitle" + id, _NewCuento.Title);
        //---

        AuxElements++;


        PlayerPrefs.SetInt("AudioCuentosGuardados", AuxElements);    
        
        Debug.Log("AudioCuentosGuardados 1: " + PlayerPrefs.GetInt("AudioCuentosGuardados"));
        
        PlayerPrefs.Save();
    }
   public int AuxElementsLeer = 0;
    void SetLeerCuentos(AudioCuentosTiles _NewCuento, string ImageSaved, string type, string title)
    {
        

        AuxElementsLeer = PlayerPrefs.GetInt("LeerCuentosGuardados");//3
        Debug.Log("AuxElementsLeer 0: " + AuxElementsLeer);

        //---
        int id = _NewCuento.ContentId;
        PlayerPrefs.SetInt("LeerID"+ AuxElementsLeer, id);
        PlayerPrefs.SetString("LeerImage" + id, ImageSaved);
        // Debug.Log("AudioImage" + cuentosList[i].ContentId);
        PlayerPrefs.SetString("LeerType" + id, _NewCuento.CategoryName);

        PlayerPrefs.SetString("LeerTitle" + id, _NewCuento.Title);
        //---

        AuxElementsLeer++;


        PlayerPrefs.SetInt("LeerCuentosGuardados", AuxElementsLeer);

        Debug.Log("LeerCuentosGuardados 1: " + PlayerPrefs.GetInt("LeerCuentosGuardados"));

        PlayerPrefs.Save();

    }



    public void DownloadAndSaveVideo(string url, string id)
    {
        StartCoroutine(DownloadVideo(url, id));
    }

    IEnumerator DownloadVideo(string url, string id)
    {

        using (WWW www = new WWW(url))
        {
            yield return www;
            string p;
#if UNITY_EDITOR
            p = Application.persistentDataPath + "\\" + "Data" + "\\";
#endif
#if UNITY_ANDROID
            p = Application.persistentDataPath +"/"+"Data/";
#endif
#if UNITY_IOS
            p = Application.persistentDataPath + "/" + "Data/";
#endif

            File.WriteAllBytes(p + id +"file.mp4", www.bytes);
          //  Debug.Log("WriteAllBytes: " + p);
            //PlayerPrefs.SetString();
        }

    }
    public void ClearPref()
    {
        PlayerPrefs.DeleteAll();

    }
    public void GetCuentosGuardados(GameObject TilePrefab, GameObject Parent)
    {
        Debug.Log("AudioCuentosGuardados: " + PlayerPrefs.GetInt("AudioCuentosGuardados"));



        for (var i = 0; i < PlayerPrefs.GetInt("AudioCuentosGuardados"); i++)
        {
            GameObject Fav = GameObject.Instantiate(TilePrefab);
            Fav.transform.SetParent(Parent.transform);
            AudioCuentosTiles ac = Fav.GetComponent<AudioCuentosTiles>();
            //PlayerPrefs.HasKey("AudioID" + i);
        //    Debug.Log("_id ---- i: " + i);
            int _id = PlayerPrefs.GetInt("AudioID" + i);
            Debug.Log("_id: " + _id);
            string _image;
            string _type = PlayerPrefs.GetString("AudioType" + _id);
            string _title = PlayerPrefs.GetString("AudioTitle" + _id);
            string _urlVideo;
#if UNITY_EDITOR
            _image = PlayerPrefs.GetString("AudioImage" + _id);

            Debug.Log("Get Image: " + _image);

            // _urlVideo = PlayerPrefs.GetString(_id.ToString());
            _urlVideo = Application.persistentDataPath + "\\" + "Data" + "\\" + _id + "file.mp4";
            //  Debug.Log("_urlVideo: " + _urlVideo);
#endif
#if UNITY_ANDROID
           
            _image = PlayerPrefs.GetString("AudioImage" + _id);
            _urlVideo = Application.persistentDataPath + "/" + "Data/" + _id + "file.mp4";
#endif

#if UNITY_IOS
            _image = PlayerPrefs.GetString("AudioImage" + _id);
            _urlVideo = Application.persistentDataPath + "/" + "Data/" + _id + "file.mp4";
#endif
            ac.SetParametersFav(_id, _image, _title, _urlVideo, "Audio", i);


        }

    }

    public void GetLeerGuardados(GameObject TilePrefab, GameObject Parent)
    {
        Debug.Log("LeerCuentosGuardados: " + PlayerPrefs.GetInt("LeerCuentosGuardados"));


        for (var i = 0; i < PlayerPrefs.GetInt("LeerCuentosGuardados"); i++)
        {
            GameObject Fav = GameObject.Instantiate(TilePrefab);
            Fav.transform.SetParent(Parent.transform);
            AudioCuentosTiles ac = Fav.GetComponent<AudioCuentosTiles>();


            int _id = PlayerPrefs.GetInt("LeerID" + i);
            string _image;
            string _type = PlayerPrefs.GetString("LeerType" + i);
            string _title = PlayerPrefs.GetString("LeerTitle" + i);
            string _urlVideo;
#if UNITY_EDITOR
            _image = PlayerPrefs.GetString("LeerImage" + _id);
            _urlVideo = Application.persistentDataPath + "\\" + "Data" + "\\" + _id + "file.mp4";
#endif
#if UNITY_ANDROID
            _image = PlayerPrefs.GetString("LeerImage" + _id);
            _urlVideo = Application.persistentDataPath + "/" + "Data/" + _id + "file.mp4";
#endif
#if UNITY_IOS
            _image = PlayerPrefs.GetString("LeerImage" + _id);
            _urlVideo = Application.persistentDataPath + "/" + "Data/" + _id + "file.mp4";
#endif
            ac.SetParametersFav(_id, _image, _title, _urlVideo, "Leer", i);
        
        

        }


    }


    public int ImagesSavedInDevice;
    //Dibujos
    public void SaveDibujosInDevice(string ImageSavedURL, int ImagesSaved)
    {
        Debug.Log("PlayerPrefs.GetInt(Dibujos:" + PlayerPrefs.GetInt("Dibujos"));
        Debug.Log("ImagesSaved" + ImagesSaved);
        
        PlayerPrefs.SetString("Dibujo" + ImagesSaved, ImageSavedURL);
        // ImagesSavedInDevice = PlayerPrefs.GetInt("Dibujos");
        //Debug.Log("ImagesSavedInDevice" + ImagesSavedInDevice);

        //for (var i = ImagesSavedInDevice; i < ImagesSaved + ImagesSavedInDevice; i++)
        //{

        //  PlayerPrefs.SetString("Dibujo" + , ImageSavedURL);
        //Debug.Log("Dibujo" + i + ": " + ImageSavedURL);

        //}

        Debug.Log("ImagesSavedInDevice" + ImagesSaved);
        PlayerPrefs.SetInt("Dibujos", ImagesSaved);
        PlayerPrefs.Save();




    }
    public void GetElementsSavedInDeviceDibujos(GameObject TilePrefab, GameObject Parent)
    {

        ImagesSavedInDevice = PlayerPrefs.GetInt("Dibujos");
        Debug.Log("ImagesSavedInDevice: " + PlayerPrefs.GetInt("Dibujos"));

       

        Debug.Log("Dibujos: " + PlayerPrefs.GetInt("Dibujos"));

        for (var i = 1; i <= PlayerPrefs.GetInt("Dibujos"); i++)
        {

            GameObject Fav = GameObject.Instantiate(TilePrefab);
            Fav.transform.SetParent(Parent.transform);
            TileDibujos ac = Fav.GetComponent<TileDibujos>();

            string _image = PlayerPrefs.GetString("Dibujo" + i);

            Debug.Log("_image: " +i+" - "+ PlayerPrefs.GetString("Dibujo" + i));
          
            ac.SetImage(_image, i);
        
        }


    }
    public bool SoundsON;

    public void SetSound()
    {
       // Debug.Log("SoundsOff!!: " + SoundsON);
        AudioSource[] sources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        for (int index = 0; index < sources.Length; ++index)
        {
            sources[index].mute = !SoundsON;
        }

        

    }
}

