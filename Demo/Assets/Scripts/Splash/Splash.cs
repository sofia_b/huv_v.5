﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Splash : MonoBehaviour {

	private float elapsedTime;
	public float TimeToWait;
	public string SceneToGO = "SCN_MainMenu";
	
	void Start () {
		elapsedTime = 0;
	}
	
	
	void Update () {
		elapsedTime += Time.deltaTime;

		if (elapsedTime > TimeToWait) {
			SceneToGo();
		}
	}

	public void SceneToGo() {
		SceneManager.LoadScene(SceneToGO);
	}
}
