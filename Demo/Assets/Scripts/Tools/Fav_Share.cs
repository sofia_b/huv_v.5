﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using UnityEngine.UI;

public class Fav_Share : MonoBehaviour
{
    public static Fav_Share instance;
    public Image ImageToShot;
    public string URLImage;
    //for android

    public string message;
   
    void Awake()
    {
        MakeInstance();
      
    }

    //method whihc make this object instance
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    //function called from a button
    public void ButtonShare()
    {

        URLImage = this.GetComponent<TileDibujos>()._urlImage;
        Debug.Log("ButtonShare");
        StartCoroutine(ShareIt());
        //CreateActivity();


    }


    // Update is called once per frame
    IEnumerator ShareIt()
    {
        yield return new WaitForEndOfFrame();


        this.GetComponent<NativeShareScript>().imgUrl = URLImage;
        this.GetComponent<NativeShareScript>().ShareBtnPress();




    }
    /*public byte[] IMGBytes;
    private IEnumerator ConvertUrlToImage(string url)
    {

        Texture2D tex;
        tex = new Texture2D(1, 1, TextureFormat.DXT1, false);
        using (WWW www = new WWW(url))
        {
            yield return www;
            www.LoadImageIntoTexture(tex);
            Rect rec = new Rect(0, 0, tex.width, tex.height);
                      
            IMGBytes = www.bytes;
            this.transform.localScale = new Vector3(1, 1, 1);

        }
    }*/
}
