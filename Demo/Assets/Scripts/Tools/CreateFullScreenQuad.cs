﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateFullScreenQuad : MonoBehaviour
{
    public RectTransform referenceArea; // we will match the size of this reference object
    private float canvasScaleFactor = 1; // canvas scaling factor (will be taken from Canvas)
    private Camera cam; // main camera reference
    public Vector2 canvasSizeAdjust = new Vector2(0, 0); // this means, "ScreenResolution.xy+screenSizeAdjust.xy" (use only minus values, to add un-drawable border on right or bottom)

    // for old GUIScaling
    private float scaleAdjust = 1.0f;
    private const float BASE_WIDTH = 800;
    private const float BASE_HEIGHT = 480;
    [Header("Overrides")]
    public float resolutionScaler = 1.0f; // 1 means screen resolution, 0.5f means half the screen resolution
    public bool overrideResolution = false;
    public int overrideWidth = 1024;
    public int overrideHeight = 768;

    private int texWidth;
    private int texHeight;
    private Touch touch; // touch reference
   // private bool wasTouching = false; // in previous frame we had touch
    private Renderer myRenderer;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;

        // calculate scaling ratio for different screen resolutions
        float _baseHeightInverted = 1.0f / BASE_HEIGHT;
        float ratio = (Screen.height * _baseHeightInverted) * scaleAdjust;
        canvasSizeAdjust *= ratio;
        CreateFullScreenQuad_();
    }
    void CreateFullScreenQuad_()
    {
        // create mesh plane, fits in camera view (with screensize adjust taken into consideration)
        Mesh go_Mesh = this.GetComponent<MeshFilter>().mesh;
        go_Mesh.Clear();


        Vector3[] referenceCorners = new Vector3[4];
       /* referenceArea = null;
        if (referenceArea) // use reference object & canvas scaling for size
        {
            if (referenceArea == null) Debug.LogError("RectTransform not assigned in " + transform.name, gameObject);

            // NOTE: this fails, if canvas is not direct parent of the referenceArea object?
            var temporaryCanvasArray = referenceArea.GetComponentsInParent<Canvas>();

            if (temporaryCanvasArray == null || temporaryCanvasArray.Length == 0) Debug.LogError("Canvas not found from ReferenceArea parent", gameObject);
            if (temporaryCanvasArray.Length > 1) Debug.LogError("More than 1 Canvas was found from ReferenceArea parent, can cause problems", gameObject);

            var referenceCanvas = temporaryCanvasArray[0]; // take first canvas
            if (referenceCanvas == null) Debug.LogError("Canvas not found from ReferenceArea parent", gameObject);

            // get current scale factor
            canvasScaleFactor = referenceCanvas.scaleFactor;

            // get vertex positions for borders
            referenceCorners[0] = new Vector3(referenceArea.offsetMin.x * canvasScaleFactor, referenceArea.offsetMin.y * canvasScaleFactor, 0);
            referenceCorners[1] = new Vector3(referenceArea.offsetMin.x * canvasScaleFactor, Screen.height + referenceArea.offsetMax.y * canvasScaleFactor, 0);
            referenceCorners[2] = new Vector3(Screen.width + referenceArea.offsetMax.x * canvasScaleFactor, Screen.height + referenceArea.offsetMax.y * canvasScaleFactor, 0);
            referenceCorners[3] = new Vector3(Screen.width + referenceArea.offsetMax.x * canvasScaleFactor, referenceArea.offsetMin.y * canvasScaleFactor, 0);

            // reset Z position and center/scale to camera view
            for (int i = 0; i < referenceCorners.Length; i++)
            {
                referenceCorners[i] = referenceCorners[i];
                referenceCorners[i].z = -cam.transform.position.z;
            }
            go_Mesh.vertices = referenceCorners;

        }
        else
        { */// just use full screen quad for main camera


            referenceCorners[0] = new Vector3(0, canvasSizeAdjust.y, cam.nearClipPlane); // bottom left
            referenceCorners[1] = new Vector3(0, cam.pixelHeight + canvasSizeAdjust.y, cam.nearClipPlane); // top left
            referenceCorners[2] = new Vector3(cam.pixelWidth + canvasSizeAdjust.x, cam.pixelHeight + canvasSizeAdjust.y, cam.nearClipPlane); // top right
            referenceCorners[3] = new Vector3(cam.pixelWidth + canvasSizeAdjust.x, canvasSizeAdjust.y, cam.nearClipPlane); // bottom right
       // }

        // move to screen
        float nearClipOffset = 0.01f; // otherwise raycast wont hit, if exactly at nearclip z


        for (int i = 0; i < referenceCorners.Length; i++)
        {
            referenceCorners[i].z = -cam.transform.position.z + nearClipOffset;
            referenceCorners[i] = cam.ScreenToWorldPoint(referenceCorners[i]);
        }


        go_Mesh.vertices = referenceCorners;

        go_Mesh.uv = new[] { new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0) };
        go_Mesh.triangles = new[] { 0, 1, 2, 0, 2, 3 };

        go_Mesh.RecalculateNormals();
        go_Mesh.RecalculateBounds();

        go_Mesh.tangents = new[] { new Vector4(1.0f, 0.0f, 0.0f, -1.0f), new Vector4(1.0f, 0.0f, 0.0f, -1.0f), new Vector4(1.0f, 0.0f, 0.0f, -1.0f), new Vector4(1.0f, 0.0f, 0.0f, -1.0f) };


        // add mesh collider
        if (gameObject.GetComponent<MeshCollider>() == null) gameObject.AddComponent<MeshCollider>();
    }
}
