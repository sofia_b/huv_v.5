﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MedaiPlayerSampleGUI_Mod : MonoBehaviour
{

	public GameObject scrMedia;

	[Header("Control buttons")]
	public Button Btn_Play;
	public Button Btn_Pause;
	public Button Btn_Stop;
	public Button Btn_Fullscreen;



	[Header("bools")]
	public bool m_bFinish = false;
	// Use this for initialization
	void Start()
	{
		
		LoadVideo(scrMedia.GetComponent<MediaPlayerCtrl_Mod>().m_strFileName);
		scrMedia.GetComponent<MediaPlayerCtrl_Mod>().OnEnd += OnEnd;
		Btn_Play.onClick.AddListener(PlayVideo);
		Btn_Play.onClick.AddListener(PlayVideo);
		Btn_Play.onClick.AddListener(PlayVideo);
		Btn_Play.onClick.AddListener(PlayVideo);

	}

	private void DontlockDevice() {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		Debug.Log("Screen.sleepTimeout: " + Screen.sleepTimeout);

	}

	// Update is called once per frame
	void Update()
	{



	}
	private void PlayVideo() {
		scrMedia.GetComponent<MediaPlayerCtrl_Mod>().Play();
		m_bFinish = false;
	}
	private void PauseVideo()
	{
		scrMedia.GetComponent<MediaPlayerCtrl_Mod>().Pause();
	}	
	private void LoadVideo(string video) {
		//scrMedia.Load("EasyMovieTexture.mp4");
		scrMedia.GetComponent<MediaPlayerCtrl_Mod>().Load(video);
		m_bFinish = false;
	}
	private void StopVideo() {
		scrMedia.GetComponent<MediaPlayerCtrl_Mod>().Stop();
	}

	private void UnloadVideo() {
		scrMedia.GetComponent<MediaPlayerCtrl_Mod>().UnLoad();
	}
	private void SeekToVideo() {
		scrMedia.GetComponent<MediaPlayerCtrl_Mod>().SeekTo(10000);
	}
	void OnGUI()
	{
	}



	void OnEnd()
	{
		m_bFinish = true;
	}
}
