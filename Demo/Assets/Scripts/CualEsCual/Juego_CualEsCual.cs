﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Juego_CualEsCual : MonoBehaviour
{
    [Header("Personajes")]
    public Sprite[] CualEsCualPersonajes;
   
    public string PersonajeImagen;
    public string PersonajeTexto;
    public GameObject PersonajeImagenGO;
    public GameObject PersonajeTextoGO;

    [Header("Final Panel")]
    public GameObject FinalPanel;
    public int RespuestasAcertadas;
    public int countValidClicks = 0;
    public int ValidAnswers;

    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        if (countValidClicks == 2) {

            ValidateResults1();

            countValidClicks = 0;
            PersonajeImagenGO.GetComponent<BTN_CualEsCual>().RestoreColor();
            PersonajeTextoGO.GetComponent<BTN_CualEsCual>().RestoreColor();
        }
        if (ValidAnswers == 4)
        {
            Debug.Log("Bieeen");
            ActiveResultPanel(true);
            PersonajeImagenGO.GetComponent<BTN_CualEsCual>().RestoreColor();
            PersonajeTextoGO.GetComponent<BTN_CualEsCual>().RestoreColor();
        }
        
    }

    public void ValidateResults1()
    {
        if (PersonajeImagen == PersonajeTexto) {
            PersonajeImagenGO.GetComponent<BTN_CualEsCual>().Check.SetActive(true);
            PersonajeTextoGO.GetComponent<BTN_CualEsCual>().Check.SetActive(true);
            ValidAnswers++;
        }
    }
    private void ActiveResultPanel(bool state) {

        FinalPanel.SetActive(state);
    }
}
