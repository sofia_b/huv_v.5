﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BTN_CualEsCual : MonoBehaviour
{
    [Header("EnumConfig")]
    public PersonajesCualEsCual NombrePersonaje;
    public TipoCualEsCual tipo;
    [Header("Other Objects")]
    public Juego_CualEsCual GameControllerCEC;
    public Image ButtonImage;     
    public GameObject Check;
    [Header("Buttons Colors")]
    public Color32 Selected;
    public Color32 NotSelected;



    void Start()
    {

       GameControllerCEC = GameObject.Find("GameControllerCEC").GetComponent<Juego_CualEsCual>();

       Button btn = this.GetComponent<Button>();
       btn.onClick.AddListener(ValidateButtonsSelected);
       // SetButtonParameters(ImageToInsert, "testeo");
    }
    public enum PersonajesCualEsCual {

        Ariel,
        Rapunzel,
        BlancaNieves,
        Aurora,
        Pájaro,
        Zorro,
        Búho,
        Oso,
        León,
        Serpiente,
        Mono,
        Cocodrilo,
        PeterPan,
        Pinocho,
        GatoConBotas,
        Pulgarcito,
        Caballo,
        Burro,
        Cabra,
        Pato
    }
    public enum TipoCualEsCual
    {
        Personaje,
        Texto
    }

    void Update()
    {

    }
    public void SetButtonParameters(Sprite image, string urlToGo)
    {
        ButtonImage.sprite = image;
       
    }
    private void ValidateButtonsSelected()
    {
        Debug.Log("Click CualEsCual button");
        GameControllerCEC.countValidClicks++;

        if (tipo == TipoCualEsCual.Personaje)
        {
            GameControllerCEC.PersonajeImagen = NombrePersonaje.ToString();
            GameControllerCEC.PersonajeImagenGO = this.gameObject;
        }
        if (tipo == TipoCualEsCual.Texto)
        {
            GameControllerCEC.PersonajeTexto = NombrePersonaje.ToString();
            GameControllerCEC.PersonajeTextoGO = this.gameObject;
        }
        this.GetComponent<Image>().color = new Color32(155,155,155,255);
    }
    public void RestoreColor() {
        this.GetComponent<Image>().color = new Color32(255, 255, 255, 255);

    }
}

