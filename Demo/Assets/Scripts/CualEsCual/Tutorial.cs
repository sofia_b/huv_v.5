﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{

    public GameObject TutorialPanel;
    public Button CloseButton;
    public Button OpenButton;

    // Start is called before the first frame update
    void Start()
    {
        CloseButton.onClick.AddListener(CloseWindow);
        OpenButton.onClick.AddListener(OpenWindow);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void CloseWindow()
    {

        TutorialPanel.gameObject.SetActive(false);
    }

    void OpenWindow()
    {

        TutorialPanel.gameObject.SetActive(true);
    }
}
