﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimatedSprites : MonoBehaviour
{
    public Sprite [] framesToAnimate;
    float framesPerSecond = 10;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        animateSprites();
    }

    void animateSprites() {


        for (int i = 0; i < framesToAnimate.Length; i++)
        {
            GetComponent<Image>().sprite = framesToAnimate[i];
            //if (i == framesToAnimate.Length - 1)
               // i = 0;
        }
    }

}

