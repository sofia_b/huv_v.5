﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Lean.Localization;
using UnityEngine.Networking;
using SimpleJSON;
using UnityEngine.UI;

public class QuestionsQuiz
{
    public string CategoryId;
    public string ContentId;
    public string TypeName;
    public string CategoryName;
    public string Question;
    
}
public class AnswersQuiz
{

    public string Answer;
    public string Value;
    public bool IsCorrect;
}


public class Services_TriviasQuiz : MonoBehaviour
{
    [Header("Questions")]
    public Text QuestionText;
    public string basicTriviaURl;//http://udistcms.com/api/portals/281/Category/16029/contents
    public int QuestionsCount;
    public int NroPregunta = 0;
    public int Correctas = 0;
    public int Incorrectas = 0;
    public List<QuestionsQuiz> QuestionsList = new List<QuestionsQuiz>();
    public string[] AllQuestions;

    [Header("Anwers")]
    public string AnswerURL;//http://udistcms.com/api/contents/questions/50469
    [Header("Result")]
    public GameObject PanelResult;
    public GameObject result1;
    public GameObject result2;
    public GameObject result3;
    public GameObject Fondo;

    [Header("Url Selected")]
    public string URL_ES;
    public string URL_EN;
    public string URL_PT;

    [Header("GameObjects")]
    public GameObject Parent;
    public GameObject quizPrefab;
    public GameObject LocalizeObject;
    public GameObject TriviaMenu;

    void Start()
    {
        StartTriviaQuiz();
    }

    public void StartTriviaQuiz()
    {

        PanelResult.SetActive(false);
        Fondo.SetActive(true);
        Answers.SetActive(true);
        TriviaMenu = GameObject.Find("Services_Trivias");
        basicTriviaURl = TriviaMenu.GetComponent<Services_Trivias>().UrlToGO;
        Debug.Log("UrlToGO: " + basicTriviaURl);
        StartCoroutine(GetTextQuiz(basicTriviaURl));
    }
    private IEnumerator GetTextQuiz(string url)
    {
        using (WWW www = new WWW(url))
        {
            yield return www;
            // Debug.Log(www.text);
            ParseQuiz(www.text);
        }
    }
    void ParseQuiz(string r)
    {
        var N = JSON.Parse(r);
        QuestionsCount = N.Count;
        AllQuestions = new string[N.Count];

        for (int i = 0; i < N.Count; i++)
        {
            //Debug.Log(N[i].ToString());
            QuestionsQuiz QQ = new QuestionsQuiz();
            QQ.CategoryId = N[i]["CategoryId"].Value;
            QQ.ContentId = N[i]["ContentId"].Value;
            QQ.TypeName = N[i]["TypeName"].Value;
            QQ.CategoryName = N[i]["CategoryName"].Value;
            QQ.Question = N[i]["Title"].Value;

            AllQuestions[i] = QQ.Question;
            //   Debug.Log(" Question: " + QQ.Question);
            // Debug.Log(" ContentId: " + QQ.ContentId);
            QuestionsList.Add(QQ);
        }

        /* for (int i = 0; i < QuestionsList.Count; i++)
           {
               Debug.Log(" QuestionsList["+ i+"]: " + QuestionsList[i].Question);
           }
        */

        StartCoroutine(GetTextAnwers("http://udistcms.com/api/contents/questions/" + QuestionsList[NroPregunta].ContentId));

    }
    public void GoToQuestion()
    {

        ClearAnswers();
        StartCoroutine(GetTextAnwers("http://udistcms.com/api/contents/questions/" + QuestionsList[NroPregunta].ContentId));
    }

    private IEnumerator GetTextAnwers(string url)
    {
        using (WWW www = new WWW(url))
        {
            yield return www;
            // Debug.Log(www.downloadHandler.text);
            ParseAnswers(www.text);
        }
    }

    void ParseAnswers(string r)
    {
        var N = JSON.Parse(r);
        string QuestionName = N["Question"].ToString();
        QuestionText.text = QuestionName.Substring(1, QuestionName.Length - 2);

        for (int i = 0; i < N["Answers"].Count; i++)
        {
            // Debug.Log(N[i].ToString());

            GameObject Tile = Instantiate(quizPrefab);
            Tile.transform.parent = Parent.transform;

            AnswersQuiz aws = new AnswersQuiz();
            // Debug.Log("answers?" + N["Answers"][i].ToString());
            aws.Answer = N["Answers"][i]["Answer"].Value;
            aws.Value = N["Answers"][i]["Value"].Value;
            aws.IsCorrect = N["Answers"][i]["IsCorrect"].AsBool;
            //  Debug.Log(" is correct: " + N["Answers"][i]["IsCorrect"].AsBool);

            Tile.gameObject.name = aws.Answer;
            Tile.GetComponent<QuizButton>().SetParameters(aws.Answer, aws.IsCorrect);

        }
    }
    public GameObject Answers;
    public void ActivePanelResult()
    {

        PanelResult.SetActive(true);
        Fondo.SetActive(false);
        Answers.SetActive(false);

        result1.SetActive(false);
        result2.SetActive(false);
        result3.SetActive(false);


        if (Correctas > QuestionsCount * 0.7f)
            result1.gameObject.SetActive(true);

        if (Correctas > QuestionsCount * 0.4f && Correctas < QuestionsCount * 0.7f)
            result2.gameObject.SetActive(true);

        if (Correctas < QuestionsCount * 0.4f)
            result3.gameObject.SetActive(true);


        //   Destroy(TriviaMenu.gameObject);

    }
    private void ClearAnswers()
    {
        //  Debug.Log("Cleaning Answers");

        foreach (Transform Go in Parent.GetComponentsInChildren<Transform>(true))
        {
            if (Go.gameObject.name != "Content")
            {
                //  Debug.Log("GO: " + Go);
                //Destroy(Go.transform);
                Go.gameObject.SetActive(false);
            }
        }

    }

    public void PlayAgain() {
        Services_Trivias ST = GameObject.Find("Services_Trivias").GetComponent<Services_Trivias>();
        ST.PlayAgain();
    
    }
}
