﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Lean.Localization;
using UnityEngine.Networking;
using SimpleJSON;
using UnityEngine.SceneManagement;
public class Services_Trivias : MonoBehaviour
{
    [Header("Url Selected")]
    public string URL_Trivias;
    public string URL_ES;
    public string URL_EN;
    public string URL_PT;
    public string UrlToGO;
    [Header("Categoryies Settings")]
    public string CategoryURL;

    [Header("Localize Settings")]
    public int Language;//ParentId ES=16016 EN=16017 PT=16018
    public string CurrentLanguage = "Spanish";

    [Header("prefabs")]
    public GameObject Parent;
    public GameObject TilePrefab;
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        CurrentLanguage = GameObject.FindGameObjectWithTag("Localization").GetComponent<PersistentLocalize>().CurrentLanguageName;
        detectLanguage();
            StartCoroutine(GetTextrivias(URL_Trivias));
    }

    void detectLanguage() {
        switch (CurrentLanguage) {
            case "Spanish":
                Language = 16016;
                break;
            case "English":
                Language = 16017;
                break;
            case "Portuguese":
                Language = 16018;
                break;
        }
    
    }
    IEnumerator GetTextrivias(string url)
    {
        using (WWW www = new WWW(url))
        {
            yield return www;
            Debug.Log(www.text);
            ParseTrivias(www.text);
        }
    }

    void ParseTrivias(string r)
    {
        var N = JSON.Parse(r);
        Debug.Log(N.Count);
        for (int i = 0; i < N.Count; i++)
        {
            int ParentId = N[i]["Categories"][0]["Category"]["ParentId"].AsInt;
            Debug.Log("N[i][ParentId]:" + N[i]["Categories"][0]["Category"]["ParentId"].ToString());
            if (ParentId == Language)
            {
                Debug.Log("entra en idioms");
                //Debug.Log(N[i].ToString());
                GameObject Tile = Instantiate(TilePrefab);
                Tile.transform.parent = Parent.transform;

                BTN_Trivias trivia = Tile.GetComponent<BTN_Trivias>();
                trivia.TriviaName = N[i]["Name"].Value;
                trivia.Active = N[i]["Active"].AsBool;
                trivia.TriviaId = N[i]["Categories"][0]["CategoryId"].Value;
                trivia.Url1 = N[i]["Url"].Value;
                Debug.Log("Trivia Url1: " + trivia.Url1);
                trivia.Url2 = N[i]["Url2"].Value;
                Debug.Log("Trivia Url2: " + trivia.Url2);
                trivia.ParentId = N[i]["Categories"][0]["Category"]["ParentId"].AsInt;
                //Debug.Log("Trivia ID:  " + N[i]["Categories"][0]["CategoryId"].Value);
                Tile.gameObject.name = trivia.TriviaName;
                trivia.SetButtonParameters(trivia.TriviaId, trivia.Url1, trivia.TriviaName, trivia.TriviaId, ParentId);
            }
        }
    }

    public void SetURLToGo(string _id) {

        UrlToGO = "http://udistcms.com/api/portals/281/Category/" + _id + "/contents";
        SceneManager.LoadScene("SCN_Trivias_Quiz");
        PersistentLocalize pl = GameObject.FindGameObjectWithTag("Localization").GetComponent<PersistentLocalize>();
        pl.TriviaSelected = _id;

    }
    public void PlayAgain()
    {

       // UrlToGO = "http://udistcms.com/api/portals/281/Category/" + _id + "/contents";
        
        SceneManager.UnloadSceneAsync("SCN_Trivias_Quiz");
        SceneManager.LoadScene("SCN_Trivias_Quiz");
        //PersistentLocalize pl = GameObject.FindGameObjectWithTag("Localization").GetComponent<PersistentLocalize>();
        // pl.TriviaSelected = _id;

    }
}


