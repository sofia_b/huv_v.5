﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class BTN_Trivias : MonoBehaviour
{
    [Header("GO")]
    public Image ButtonImage;
    public Sprite ImageToInsert;
    

    [Header("Json Parameters")]
    public string TriviaId;
    public string TriviaName;
    public string Url1;
    public string Url2;
    public int ParentId;

    public string UrlToGO;

    public bool Active;
  
   

    void Start()
    {
        Button btn = this.GetComponent<Button>();
        btn.onClick.AddListener(GoToTrivia);
       // SetButtonParameters(ImageToInsert, "testeo");
    }
    void GoToTrivia() {
        StartCoroutine(GotToTriviaSelected());


    }
    public void SetButtonParameters(string id, string _img, string _name, string _urlToOpen, int _parentId)
    {
        StartCoroutine(ConvertUrlToImage(_img));
        UrlToGO = _urlToOpen;
        TriviaName = _name;
        TriviaId = id;
        ParentId = _parentId;
    }

   
    private IEnumerator GotToTriviaSelected()
    {
        yield return new WaitForSeconds(0.5f);

        Debug.Log("Click Trivia button: " + UrlToGO);
        GameObject ServicesTrivia = GameObject.Find("Services_Trivias");
       
        ServicesTrivia.GetComponent<Services_Trivias>().SetURLToGo(UrlToGO);
    }


    private IEnumerator ConvertUrlToImage(string url)
    {

        Texture2D tex;
        tex = new Texture2D(1, 1, TextureFormat.DXT1, false);
        using (WWW www = new WWW(url))
        {
            yield return www;
            www.LoadImageIntoTexture(tex);
            Rect rec = new Rect(0, 0, tex.width, tex.height);
            Sprite sprite = Sprite.Create(tex, rec, new Vector2(0, 0), 1);
            ButtonImage.sprite = sprite;

        }
    }
}

