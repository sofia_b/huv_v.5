﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Lean.Localization;

public class QuizButton : MonoBehaviour
{
    public Text Name;
    public bool isCorrect;
    private Button btn;
    public Color CorrectColor;
    public Color WrongColor;
    public GameObject ServiceQuiz;
    public GameObject[] Answers;



    // Start is called before the first frame update
    void Start()
    {
        ServiceQuiz = GameObject.FindWithTag("ServiceQuiz");
        btn = this.GetComponent<Button>();
        btn.onClick.AddListener(validateAnwer);
        Answers = GameObject.FindGameObjectsWithTag("Answers");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void UnableAnswers() {

        foreach (GameObject ans in Answers) {
            //  ans.GetComponent<Button>().interactable = false;
            ans.GetComponent<Button>().enabled = false;
        }
    }
    private void validateAnwer() {
        //  Debug.Log("Is Correct?: " + isCorrect);
        UnableAnswers();
        if (isCorrect)
        {
            this.GetComponent<Image>().color = CorrectColor;
            ServiceQuiz.GetComponent<Services_TriviasQuiz>().Correctas++;
        }
        else
        {
            this.GetComponent<Image>().color = WrongColor;
            ServiceQuiz.GetComponent<Services_TriviasQuiz>().Incorrectas++;
        }
        
        //Debug.Log("ServiceQuiz.GetComponent<Services_TriviasQuiz>().NroPregunta: "+ ServiceQuiz.GetComponent<Services_TriviasQuiz>().NroPregunta);
        if (ServiceQuiz.GetComponent<Services_TriviasQuiz>().NroPregunta < ServiceQuiz.GetComponent<Services_TriviasQuiz>().AllQuestions.Length -1)
        {            
            StartCoroutine(GoToNextQuestion());
            Answers = GameObject.FindGameObjectsWithTag("Answers");
        }
        else {
            Debug.Log("Fin");
            StartCoroutine(ShowResult());
        }
    }

    public void SetParameters(string _name, bool _isCorrect) {
     // Debug.Log("_isCorrect: " + _isCorrect);
        
        Name.text = _name;
        isCorrect = _isCorrect;

    }

    IEnumerator GoToNextQuestion() {
        yield return new WaitForSeconds(1);
        ServiceQuiz.GetComponent<Services_TriviasQuiz>().NroPregunta++;
        ServiceQuiz.GetComponent<Services_TriviasQuiz>().GoToQuestion();
    }

    IEnumerator ShowResult()
    {
        yield return new WaitForSeconds(1);
        ServiceQuiz.GetComponent<Services_TriviasQuiz>().ActivePanelResult();
    }
}






