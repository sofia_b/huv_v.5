﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleTranslation : MonoBehaviour {
	public Sprite [] TranslatedTitles;
	public string CurrentLanguage= "";
	public int CurrentIntLanguage =0;
	public Image titleImage;
	
	PersistentLocalize pl;
	// Use this for initialization
	void Start () {

		pl = GameObject.FindGameObjectWithTag("Localization").GetComponent<PersistentLocalize>();
		CurrentLanguage = pl.CurrentLanguageName;
		CurrentIntLanguage = pl.ActualLanguageInt;
	//	Debug.Log("CurrentLanguage: " + CurrentLanguage);
		//Debug.Log("CurrentIntLanguage: " + CurrentIntLanguage);

		if (CurrentLanguage == "Spanish")
		{
			titleImage.sprite = TranslatedTitles[0];
			this.GetComponent<Image>().sprite = this.GetComponent<AppButton>().LaguageImages[0];
			this.GetComponent<AppButton>().ActualLanguage = 0;
		}
		if (CurrentLanguage == "Portuguese")
		{
			titleImage.sprite = TranslatedTitles[1];
			this.GetComponent<Image>().sprite = this.GetComponent<AppButton>().LaguageImages[1];
			this.GetComponent<AppButton>().ActualLanguage = 1;
		}
		if (CurrentLanguage == "English")
		{
			titleImage.sprite = TranslatedTitles[2];
			this.GetComponent<Image>().sprite = this.GetComponent<AppButton>().LaguageImages[2];
			this.GetComponent<AppButton>().ActualLanguage = 2;

		}
		this.GetComponent<Button>().onClick.AddListener(TranslateIt);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void TranslateIt() {
		StartCoroutine(titleTT());
	}
	IEnumerator titleTT() {
		yield return new WaitForSeconds(0);

		translateTitle();
	}
	public void translateTitle() {

		CurrentIntLanguage = pl.ActualLanguageInt;
		titleImage.sprite = TranslatedTitles[pl.ActualLanguageInt];
	}
}
