﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageButtonGames : MonoBehaviour {
	
	public string CurrentLanguage;
	public int CurrentIntLanguage;
	PersistentLocalize pl;

	void Start () {
		StartCoroutine(setLan());



	}
	
	// Update is called once per frame
	void Update () {
		
	}
	IEnumerator setLan() {
		yield return new WaitForSeconds(0.3f);
		pl = GameObject.FindGameObjectWithTag("Localization").GetComponent<PersistentLocalize>();
		CurrentLanguage = pl.CurrentLanguageName;
		CurrentIntLanguage = this.GetComponent<AppButton>().ActualLanguage;
	//	Debug.Log("CurrentLanguage: " + CurrentLanguage);
		//Debug.Log("CurrentIntLanguage: " + CurrentIntLanguage);

		if (CurrentLanguage == "Spanish")
		{
			this.GetComponent<Image>().sprite = this.GetComponent<AppButton>().LaguageImages[0];
			this.GetComponent<AppButton>().ActualLanguage = 0;
		}
		if (CurrentLanguage == "Portuguese")
		{
			this.GetComponent<Image>().sprite = this.GetComponent<AppButton>().LaguageImages[1];
			this.GetComponent<AppButton>().ActualLanguage = 1;
		}
		if (CurrentLanguage == "English")
		{
			this.GetComponent<Image>().sprite = this.GetComponent<AppButton>().LaguageImages[2];
			this.GetComponent<AppButton>().ActualLanguage = 2;

		}
	}
}
