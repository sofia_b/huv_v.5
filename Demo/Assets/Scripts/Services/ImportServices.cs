﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using SimpleJSON;
using System.IO;
//using Lean.Localization;

public class ImportServices : MonoBehaviour
{
    public bool IsCategoriesNeeded = true;
    [Header("Url Selected")]
    public string URL_ES;
    public string URL_EN;
    public string URL_PT;
    public List<String> Categories_;
    [Header("Categoryies Settings")]
    public string CategoryURL;
    public string CategoriesURL;
    [Header("prefabs")]
    public GameObject Parent;
    public GameObject TilePrefab;
    public GameObject LocalizeObject;

    // Start is called before the first frame update
    void Start()
    {
        //CurrentLanguage = GameObject.FindGameObjectWithTag("Localization").GetComponent<PersistentLocalize>().CurrentLanguageName;
        LocalizeObject = GameObject.FindGameObjectWithTag("Localization");
        string LanName = LocalizeObject.GetComponent<PersistentLocalize>().CurrentLanguageName;
        if (LanName == "Spanish")
            StartCoroutine(GetTextAudioCuentos(URL_ES));
        if (LanName == "English")
            StartCoroutine(GetTextAudioCuentos(URL_EN));
        if (LanName == "Portuguese")
            StartCoroutine(GetTextAudioCuentos(URL_PT));

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator GetTextAudioCuentos(string url)
    {
        using (WWW www = new WWW(url))
        {
            yield return www;
          //  Debug.Log(www.text);
            ParseAudioCuentos(www.text);
        }
    }

    void ParseAudioCuentos(string r)
    {
        var N = JSON.Parse(r);

        for (int i = 0; i < N.Count; i++)
        {
            //Debug.Log(N[i].ToString());
            Vector3 TilePos = new Vector3(0, 0, 0);
            Quaternion TileRot = new Quaternion(0, 0, 0, 0);
            //GameObject Tile = (GameObject)Instantiate(TilePrefab);//, TilePos, TileRot);
            GameObject Tile = Instantiate(TilePrefab);//, TilePos, TileRot);
            //Tile.transform.localScale = new Vector3(1, 1, 1);
            Tile.transform.SetParent(Parent.transform);
       
            AudioCuentosTiles cuentos = Tile.GetComponent<AudioCuentosTiles>();
           
            cuentos.Id = N[i]["Id"].AsInt;
            cuentos.ContentId = N[i]["ContentId"].AsInt;
            cuentos.TypeName = N[i]["TypeName"].Value;
            cuentos.CategoryName = N[i]["CategoryName"].Value;
            cuentos.Url = N[i]["Url"].Value;
            cuentos.ThumbnailOriginallUrl = N[i]["ThumbnailOriginallUrl"].Value;
            cuentos.ThumbnailSmallUrl = N[i]["ThumbnailSmallUrl"].Value;
            cuentos.ThumbnailMediumUrl = N[i]["ThumbnailMediumUrl"].Value;
            cuentos.ThumbnailLargeUrl = N[i]["ThumbnailLargeUrl"].Value;
            cuentos.Title = N[i]["Title"].Value;

            Tile.gameObject.name = cuentos.Title;
            float tiletransformZ = Tile.transform.position.z;
            Vector3 TileScale = Tile.transform.localScale;
            TileScale = new Vector3(1,1,1);
            tiletransformZ = 0;
            cuentos.SetParameters(cuentos.Id, cuentos.ThumbnailMediumUrl, cuentos.Title, cuentos.Url);
        }
    }
        

    IEnumerator GetTextCategories(string url)
    {
        using (WWW www = new WWW(url))
        {
            yield return www;
            Debug.Log(www.text);

            
            ParseCategories(www.text);
        }
    }
    void ParseCategories(string r)
    {
       // Debug.Log(" Info Parse");
        Categories cat = new Categories();

        var N = JSON.Parse(r);

        for (int i = 0; i < N.Count; i++)
        {
            //Debug.Log(N[i].ToString());
            cat.Id = N[i]["Id"].Value;
            //Debug.Log("cat.Id: " + cat.Id);
            cat.Name = N[i]["Name"].Value;
          //  Debug.Log("cat.Name: " + cat.Name);
            cat.Description = N[i]["Description"].Value;
            //Debug.Log("cat.Description: " + cat.Description);

            

        }


     }
}


[Serializable]
public class Categories 
{
    public string Id;
    public string Name;
    public string Description;
    

    public static Categories CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<Categories>(jsonString);
    }
}

[Serializable]
public class AudioCuentosCategory
{

    public int Id;
    public int ContentId;
    public string TypeName;
    public string CategoryName;
    public string Url;
    public string ThumbnailOriginallUrl;
    public string ThumbnailSmallUrl;
    public string ThumbnailMediumUrl;
    public string ThumbnailLargeUrl;
    public string Title;

    public AudioCuentosCategory()
    {
    }
    public static AudioCuentosCategory CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<AudioCuentosCategory>(jsonString);
    }

}
