﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Lean.Localization;
using UnityEngine.Networking;
using SimpleJSON;

public class ImportServices_leer : MonoBehaviour
{
    [Header("Url Selected")]
    public string URL_ES;
    public string URL_EN;
    public string URL_PT;
  
    [Header("Categoryies Settings")]
    public string CategoryURL;
  
    [Header("prefabs")]
    public GameObject Parent;
    public GameObject TilePrefab;
    public GameObject LocalizeObject;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(GetTextAudioCuentos(URL_ES));/*
        if (LeanLocalization.CurrentLanguage == "Spanish")
            StartCoroutine(GetTextAudioCuentos(URL_ES));
        if (LeanLocalization.CurrentLanguage == "English")
            StartCoroutine(GetTextAudioCuentos(URL_EN));
        if (LeanLocalization.CurrentLanguage == "Portuguese")
            StartCoroutine(GetTextAudioCuentos(URL_PT));
            */
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator GetTextAudioCuentos(string url)
    {
        using (WWW www = new WWW(url))
        {
            yield return www;
            Debug.Log(www.text);
            ParseAudioCuentos(www.text);
        }
    }

    void ParseAudioCuentos(string r)
    {
        var N = JSON.Parse(r);

        for (int i = 0; i < N.Count; i++)
        {
            Debug.Log(N[i].ToString());

            GameObject Tile = Instantiate(TilePrefab);
            Tile.transform.parent = Parent.transform;

            AudioCuentosTiles cuentos = Tile.GetComponent<AudioCuentosTiles>();

            cuentos.Id = N[i]["Id"].AsInt;
            cuentos.ContentId = N[i]["ContentId"].AsInt;
            cuentos.TypeName = N[i]["TypeName"].Value;
            cuentos.CategoryName = N[i]["CategoryName"].Value;
            cuentos.Url = N[i]["Url"].Value;
            cuentos.ThumbnailOriginallUrl = N[i]["ThumbnailOriginallUrl"].Value;
            cuentos.ThumbnailSmallUrl = N[i]["ThumbnailSmallUrl"].Value;
            cuentos.ThumbnailMediumUrl = N[i]["ThumbnailMediumUrl"].Value;
            cuentos.ThumbnailLargeUrl = N[i]["ThumbnailLargeUrl"].Value;
            cuentos.Title = N[i]["Title"].Value;

            Tile.gameObject.name = cuentos.Title;
            cuentos.SetParameters(cuentos.Id, cuentos.ThumbnailMediumUrl, cuentos.Title, cuentos.Url);
        }
    }


    IEnumerator GetTextCategories(string url)
    {
        using (WWW www = new WWW(url))
        {
            yield return www;
            Debug.Log(www.text);


            ParseCategories(www.text);
        }
    }
    void ParseCategories(string r)
    {
        Debug.Log(" Info Parse");
        Categories cat = new Categories();

        var N = JSON.Parse(r);

        for (int i = 0; i < N.Count; i++)
        {
            Debug.Log(N[i].ToString());
            cat.Id = N[i]["Id"].Value;
            Debug.Log("cat.Id: " + cat.Id);
            cat.Name = N[i]["Name"].Value;
            Debug.Log("cat.Name: " + cat.Name);
            cat.Description = N[i]["Description"].Value;
            Debug.Log("cat.Description: " + cat.Description);
        }
    }
}
