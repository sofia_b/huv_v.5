﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using unitycoder_MobilePaint_samples;
using Localization;




public class AppButton : MonoBehaviour
{
    [Header("Flow Configuration")]
    public ScenesToLoad SceneToGo;
    public bool NeedsToShowText = true;
    private Text ButtonName;
    public bool isAnotherButton;
    public Button ClickableButton;


    [Header("Language Configuration")]
    public string [] LaguageNames;
    public Sprite [] LaguageImages;
    public int ActualLanguage = 0;
    public GameObject LeanLocalize;
    public enum ScenesToLoad
    {
        SplashSreen,
        MainMenu,
        AudioCuentos,
        AprendiendoALeer,
        Trivias,
        Trivias_Quiz,
        Juegos,
        HoraDePintar,
        HoraDePintar_Paint,
        Imaginario,
        CualEsCual,
        CualEsCual_juego,
        CualEsCual_juego01,
        CualEsCual_juego02,
        CualEsCual_juego03,
        CualEsCual_juego04,
        CualEsCual_juego05,
        Favoritos,
        Informacion,
        Idioma,
        Volver,
        SaveImage,
        None,
        TiviaAgain

    }

    void setActualLanguague() { 
       
    
    }
    void Start()
    {
     
        if (NeedsToShowText)
        {
          //  ButtonName = this.GetComponentInChildren<Text>();
           // ButtonName.text = SceneToGo.ToString();
        }

        LeanLocalize = GameObject.FindWithTag("Localization");
        string langName = LeanLocalize.GetComponent<PersistentLocalize>().CurrentLanguageName;
        Debug.Log("langName: " + langName);
        if (langName == "Spanish") {
            LeanLocalize.GetComponent<PersistentLocalize>().ActualLanguageInt = 0;
            ActualLanguage = 0;
        }
        if (langName == "Portuguese")
        {
            LeanLocalize.GetComponent<PersistentLocalize>().ActualLanguageInt = 1;
            ActualLanguage = 1;
        }
        if (langName == "English")
        {
            LeanLocalize.GetComponent<PersistentLocalize>().ActualLanguageInt = 2;
            ActualLanguage = 2;
        }
        LocalizationService.Instance.Localization = langName;
        Debug.Log("ActualLanguage: " + ActualLanguage);
        verifyButton();
    }

    private void verifyButton()
    {
        Button btn;
        if (isAnotherButton)
        {
            btn = ClickableButton.GetComponent<Button>();
        }
        else
        {
            btn = this.GetComponent<Button>();
        }

        if (SceneToGo != ScenesToLoad.Idioma && SceneToGo != ScenesToLoad.Volver && SceneToGo != ScenesToLoad.SaveImage)
        {
            btn.onClick.AddListener(GotToNextScene);
        }

        if (SceneToGo == ScenesToLoad.SaveImage)
        {
            btn.onClick.AddListener(this.GetComponent<SaveImageToFile>().TakeIt);
         

        }
        if (SceneToGo == ScenesToLoad.Idioma)
        {
            //Debug.Log("Language: " + LaguageImages[LeanLocalize.GetComponent<PersistentLocalize>().ActualLanguageInt]);
            this.GetComponent<Image>().sprite = LaguageImages[LeanLocalize.GetComponent<PersistentLocalize>().ActualLanguageInt];

            btn.onClick.AddListener(SetLanguage);
        }
        if (SceneToGo == ScenesToLoad.TiviaAgain) {
          //  SetURLToGo


        }

    }

        private void SetLanguage() {
       

        if (ActualLanguage < LaguageNames.Length -1)
        {
            ActualLanguage++;
        }
        else
        {
            ActualLanguage = 0;
        }

       // LeanLocalize.GetComponent<LeanLocalization>().SetCurrentLanguage(LaguageNames[ ActualLanguage]);
        this.GetComponent<Image>().sprite = LaguageImages[ActualLanguage];

        LeanLocalize.GetComponent<PersistentLocalize>().CurrentLanguageName = LaguageNames[ActualLanguage];
        LeanLocalize.GetComponent<PersistentLocalize>().CurrentLanguageImage = LaguageImages[ActualLanguage];
        LeanLocalize.GetComponent<PersistentLocalize>().ActualLanguageInt = ActualLanguage;
        PlayerPrefs.SetString("Language", LaguageNames[ActualLanguage]);
     //   Debug.Log("ActualLanguage:" + LaguageNames[ActualLanguage]);
        LocalizationService.Instance.Localization = LaguageNames[ActualLanguage];



    }
    
    private void GotToNextScene() {
        StartCoroutine(gotosceneSecond());
        
    }
    IEnumerator gotosceneSecond() {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("SCN_" + SceneToGo.ToString());

    }
    
    public void SetScene(string valueString) {       
         SceneToGo = (ScenesToLoad) System.Enum.Parse(typeof(ScenesToLoad), valueString);        
    }
    
    void Update()
    {
        
    }

    public void SetURLToGoQuiz(string _id)
    {       
       
        PersistentLocalize pl = GameObject.FindGameObjectWithTag("Localization").GetComponent<PersistentLocalize>();
        pl.TriviaSelected = _id;

    }
}

