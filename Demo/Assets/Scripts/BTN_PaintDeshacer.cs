﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using unitycoder_MobilePaint;

public class BTN_PaintDeshacer : MonoBehaviour {
	
	public MobilePaint mobilePaintSelected;
	public PaintManager PM_;
	// Use this for initialization
	void Start () {
		this.GetComponent<Button>().onClick.AddListener(SetMObilePaintSelected);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void SetMObilePaintSelected() {
		mobilePaintSelected = PM_.mobilePaintReference;
		mobilePaintSelected.ClearImage();
	}
}
