﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class VideosMain : MonoBehaviour {

	CategoryService categoryService;


	public GameObject item;
	public GameObject itemUp;
	public GameObject itemDown;
	public GameObject home;
	public GameObject back;

	private ArrayList videos;
	private int maxColumnas = 8;//2;//SMB: Optimized on 8
	private int maxFilas = 4;//SMB: Optimized on 4
	public GameObject Sphere;
	public GameObject SphereCat;
	public string UrlBG;//SMB
	public int catId;//SMB
	Renderer renderer; //SMB


	//SMB{
	public void SaveTextureToFile(Texture2D texturetosave, string path){
		// to store image
		var bytes = texturetosave.EncodeToJPG ();
		System.IO.File.WriteAllBytes (path, bytes);
	}

	public Texture2D LoadTextureFromFile(string path){
		// To get image
		var bytesRead = System.IO.File.ReadAllBytes (path);
		Texture2D myTexture = new Texture2D(4, 4, TextureFormat.DXT1, false);
		//Texture2D myTexture = new Texture2D (720, 271);
		myTexture.LoadImage(bytesRead);
		return myTexture;
	}
	public string DirectoryToDownload="DownloadCatBG";//The Folder to storage the files download
	public string pathurl;

	public string GetDirectoryPath(){
		return Application.persistentDataPath + "/" + DirectoryToDownload;
	}

	public string ConvertURLtoFileName(string URLk){//COMPLETE
		string VideoName = URLk;
		VideoName = VideoName.Replace("http://","");
		VideoName = VideoName.Replace("/","-");
		VideoName = GetDirectoryPath() + "/" + VideoName;
		return VideoName;
	}

	public bool CheckDirectory(){
		return System.IO.Directory.Exists (GetDirectoryPath());
	}

	public void CreateDirectory(){
		if (!CheckDirectory ()) {
			System.IO.Directory.CreateDirectory(GetDirectoryPath());
		}
	}

	public void DeleteDirectory(){
		if (CheckDirectory ()) {
			System.IO.Directory.Delete(GetDirectoryPath(),true);
		}
	}

	public void ClearDirectory(){
		DeleteDirectory ();
		CreateDirectory ();
	}

	public bool FindFilesFromURL(string URLx){//To check
		bool Existing = System.IO.File.Exists(ConvertURLtoFileName(URLx));
		return Existing;
	}

	public void DeleteFileFromURL(string URLz){
		if(FindFilesFromURL(URLz)){
			System.IO.File.Delete(ConvertURLtoFileName(URLz));
		}
	}

	//SMB}


	// Use this for initialization
	void Start () {
		
		UrlBG = PlayerPrefs.GetString("Category_URL2");//----SMB
		pathurl = UrlBG;
		//---SMB
		SphereCat = GameObject.FindGameObjectWithTag ("SphereCategory");
		//----SMB
		//SMB----------------------------------------HERE
		StartCoroutine (GetBG ());//----SMB


		catId = PlayerPrefs.GetInt ("Category_id", 0);

		categoryService = gameObject.AddComponent<CategoryService>();
		categoryService.myDelegate = ShowVideos;
		categoryService.read(catId);
		SelectItem homeSelectItem = (SelectItem)home.GetComponent(typeof(SelectItem));
		homeSelectItem.itemSelected = OpenVideo;

		SelectItem backSelectItem = (SelectItem)back.GetComponent(typeof(SelectItem));
		backSelectItem.itemSelected = OpenVideo;
//		GetComponent<Cardboard>().OnBackButton += ()=>{ SceneManager.LoadScene ("Categories"); };
	

		//Debug.Log("Nombre Objeto = " + catId);//SMB: Debug anulado


	}


	IEnumerator GetBG() {
		// Start a download of the given URL

		CreateDirectory ();
		if(FindFilesFromURL(pathurl)){//if is pre-exist, only load the file, no reload the url if they exist
			GetComponent<Renderer> ().material.mainTexture = new Texture2D(4, 4, TextureFormat.RGB24, false);
			GetComponent<Renderer> ().material.mainTexture = LoadTextureFromFile (ConvertURLtoFileName (pathurl));
			//GetComponent<Renderer> ().material.mainTexture = new Texture2D(4, 4, TextureFormat.DXT1, false);
			//return LoadTextFromFile (ConvertURLtoFileName (url));
		}else{


		WWW www = new WWW(UrlBG);
		
		// Wait for download to complete
		yield return www;
		
		//Debug.Log ("VIDEOS MAIN =====>>>> GetBG: " + www.ToString ());

		// assign texture
		SphereCat.gameObject.GetComponent<Renderer> ().material.mainTexture = www.texture;

			SaveTextureToFile(SphereCat.gameObject.GetComponent<Renderer> ().material.mainTexture as Texture2D, ConvertURLtoFileName(pathurl));//SMB: This save the tile image
		}



	}

	string textCat;
	int lineLength;//SMB
	// Update is called once per frame
	void Update () {
	
	}

	//SMB{
	public float heightButton;
	//SMB}
	public int PageOfVideos = 1;

	void ShowVideos(ArrayList list){
		videos = list;
		int size = videos.Count;
		if (size <= maxColumnas * maxFilas) {
			//SMB: Could be render on the same videowall
			PageOfVideos = 1;
		} else {
			//SMB: Need more than one videowall to show the total videos
			//SMB: Need to add buttoms!!!
			//SMB: PageOfVideos = 1;//SMB:????
			//SMB: 
		}

		int filas = size / maxColumnas;
		filas = size % maxColumnas != 0 ? filas + 1 : filas;

		int columnas = size / filas;
		if (filas > 1){
			columnas = size % filas != 0 ? columnas + 1 : columnas;
		}

		//Debug.Log("filas:"+ filas);//SMB: Debug anulado
		//Debug.Log("columnas:"+ columnas);//SMB: Debug anulado

		//SMB BEGIN: Mathematical calculation for correction the heigth
		float DiffHeightFilas=0f;//SMB: Add to regularized the heigth
		if (filas > maxFilas) {
			DiffHeightFilas = (maxFilas - 1) * 0.15f;
		} else {
			DiffHeightFilas = (filas - 1) * 0.15f;
		}
		//SMB END.

		int leftDegree = 20 * (1 - columnas) + 180;
		int HeigthInPage = 0;//SMB: To Calculate height by the page
		for (int fila = 0; fila < filas /*&& fila < maxFilas*/; fila++){
			HeigthInPage++;//SMB: Set the heigth
			if (HeigthInPage > maxFilas) {
				DiffHeightFilas = DiffHeightFilas - 0.6f;
				HeigthInPage = 1;
				GameObject PageUp = Instantiate (itemUp, new Vector3 (0, (0.3f*fila)+heightButton-DiffHeightFilas, 0), Quaternion.Euler(new Vector3 (0, leftDegree + (0), 0)))  as GameObject;
				GameObject PageDown = Instantiate (itemDown, new Vector3 (0, (0.3f*fila)+heightButton-DiffHeightFilas, 0), Quaternion.Euler(new Vector3 (0, leftDegree + (0), 0)))  as GameObject;
			}
			for(int columna = 0; columna < columnas && columna < (size - (fila*columnas)); columna++){
				
				int index = columna + fila * columnas;
				Content cat = videos [index] as Content;

				//SMB: THIS MODIFFY THE SECTION FROM HEIGTH

				GameObject card = Instantiate (item, new Vector3 (0, (0.3f*fila)+heightButton-DiffHeightFilas, 0), Quaternion.Euler(new Vector3 (0, leftDegree + (40 * columna), 0)))  as GameObject;
				//SMB: END OF...

				TextMesh title = (TextMesh)card.GetComponentInChildren (typeof(TextMesh));

				title.text = cat.title;

				//SMB{
				Debug.Log("VIDEO MAIN ===>>> ITEM: '"+item+"' Category: '"+cat.id+" - "+cat.title+" - "+cat.url+"' card: '"+card.name+"'");

				card.name = "Vid" + columna + "-" + cat.id + "-" + cat.title;

				card.tag = "Video";
				card.gameObject.AddComponent<VideoURL>();
				card.gameObject.GetComponent<VideoURL>().URL = cat.url;

				card.gameObject.GetComponent<SelectItem>().deb = GameObject.Find("DebugText");//To Debug
				card.gameObject.GetComponent<SelectItem>().VideoController = GameObject.Find("Controller");//To Debug
				//if (card.gameObject.GetComponent<SelectItem> ().VideoController != null) {
				//	card.gameObject.GetComponent<SelectItem> ().VideoController.GetComponent<VideoPlayController> ().RefrshButtons ();
				//}
				//}SMB

				//------SMB
			
				 
				
				textCat = title.text;

				//----
				//SMB...
			//	Debug.Log("=========>>>>>>title.text:"+title.text);
				
				if(title.text.Length<80){
					lineLength = 15;
					title.characterSize = 1f;
				}else{
					if(title.text.Length<278){
						lineLength = 30;
						title.characterSize = 0.5f;
					}else{
						//if(title.text.Length<1097){
						lineLength = 60;
						title.characterSize = 0.25f;
						// }else{
						
						// }
					}
				}
				
				string TextoTemp="";
				int s=0;

				for( int i = 0; i < title.text.Length; i++){
					//Debug.Log("=========  >>>>>> i:"+i);
					if(s>=lineLength){
						if(title.text[i]==' '){
							//title.text[i]="\n";
							//title.text[i]="\r\n";
							TextoTemp=TextoTemp+"\r\n";
							s=0;
						}else{
							TextoTemp=TextoTemp+title.text[i];
						}
					}else{
						TextoTemp=TextoTemp+title.text[i];
					}
					s++;
				}
				title.text=TextoTemp;
				//SMB...
				//----
				//------SMB


				LoadImage imageLoader = (LoadImage) card.GetComponent(typeof(LoadImage));
				imageLoader.DownloadImage(cat.thumbnail);
				SelectItem selector = (SelectItem)card.GetComponent (typeof(SelectItem));
				selector.index = index;
				selector.itemSelected = OpenVideo;

				//Debug.Log("degree:"+ leftDegree + (40 * columna));//SMB:comentareo de Debugs previos
			}
		}
	}
	public GameObject MPause;
	bool Checkin= false;

	public void OpenVideo(int index){

		if (index == -1){
			SceneManager.LoadScene ("Welcome");
			return;
		}

		if (index == -2){
			//Cardboard.SDK.Recenter();//SMB: Act to re-center the camera
			SceneManager.LoadScene ("Categories");
			return;
		}


		Content video = videos [index] as Content;
		PlayerPrefs.SetString("ContentURL", video.url);

		//Cardboard.SDK.Recenter();//SMB: Act to re-center the camera
		SceneManager.LoadScene ("Player");
	}
}
