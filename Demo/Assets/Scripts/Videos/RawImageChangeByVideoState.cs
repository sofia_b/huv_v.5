﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RawImageChangeByVideoState : MonoBehaviour {
	public MediaPlayerCtrl_Mod mpcMod;
	public Color ActiveColor;
	public Color InactiveColor;
	public Text Debug;
	public Text Debug2;
	public RawImage raw;
	public bool isWindowActive = false;
	
	// Use this for initialization
	void Start() {
		ChangeRawState(false);
	}

	// Update is called once per frame
	void Update() {
	//	Debug.text = mpcMod.m_CurrentState.ToString() + "\n" + mpcMod.m_strFileName;
		if (!isWindowActive) { ChangeRawState(false); }
		else
		{
			if (mpcMod.m_CurrentState == MediaPlayerCtrl_Mod.MEDIAPLAYER_STATE.PLAYING)
				ChangeRawState(true);
			if (mpcMod.m_CurrentState == MediaPlayerCtrl_Mod.MEDIAPLAYER_STATE.NOT_READY)
				ChangeRawState(false);
			if (mpcMod.m_CurrentState == MediaPlayerCtrl_Mod.MEDIAPLAYER_STATE.STOPPED)
				ChangeRawState(false);
			if (mpcMod.m_CurrentState == MediaPlayerCtrl_Mod.MEDIAPLAYER_STATE.END)
				ChangeRawState(false);
			if (mpcMod.m_CurrentState == MediaPlayerCtrl_Mod.MEDIAPLAYER_STATE.READY)
				ChangeRawState(false);
			if (mpcMod.m_CurrentState == MediaPlayerCtrl_Mod.MEDIAPLAYER_STATE.PAUSED)
				ChangeRawState(false);
			if (mpcMod.m_CurrentState == MediaPlayerCtrl_Mod.MEDIAPLAYER_STATE.ERROR)
				ChangeRawState(false);
		}
	}
	public void ChangeRawState(bool state) {
		if (state == true)
		{
			raw.GetComponent<RawImage>().color = ActiveColor;
		}
		if (state == false)
			raw.GetComponent<RawImage>().color = InactiveColor;

	}

	
}

