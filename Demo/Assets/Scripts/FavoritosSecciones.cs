﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FavoritosSecciones : MonoBehaviour
{
    [Header("Botones")]
    public GenericButton AudioCuentos;
    public GenericButton AprendiendoALeer;
    public GenericButton Dibujos;
    public Color32 [] StateColors;

    [Header("Menues")]
    public GameObject MenuAudioCuentos;
    public GameObject MenuAprendiendoALeer;
    public GameObject MenuDibujos;

    void Start()
    {
        AudioCuentos.MenuToOpen = MenuAudioCuentos;
        AprendiendoALeer.MenuToOpen = MenuAprendiendoALeer;
        Dibujos.MenuToOpen = MenuDibujos;
    }


    public void closeOtherMenus() {
        MenuAudioCuentos.SetActive(false);
        MenuAprendiendoALeer.SetActive(false);
        MenuDibujos.SetActive(false);

        AudioCuentos.GetComponent<GenericButton>().closeOtherMenus();
        AprendiendoALeer.GetComponent<GenericButton>().closeOtherMenus();
        Dibujos.GetComponent<GenericButton>().closeOtherMenus();
    }

    private void OnStateChange() {


    }
    
    void Update()
    {
        
    }


}

