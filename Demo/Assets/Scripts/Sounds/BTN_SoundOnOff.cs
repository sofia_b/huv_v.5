﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BTN_SoundOnOff : MonoBehaviour {

	public Sprite SoundON;
	public Sprite SoundOff;
	public bool isSoundActive = true;
	PersistentLocalize pl;
	// Use this for initialization
	string b;
	void Start()
	{
		pl = GameObject.FindGameObjectWithTag("Localization").GetComponent<PersistentLocalize>();
		isSoundActive = pl.SoundsON;


		//isSoundActive = pl.SoundsON;
		Debug.Log("!! --> isSoundActive: " + isSoundActive);

		pl.SetSound();
		setIcon();
		this.GetComponent<Button>().onClick.AddListener(ChangeState);
	}

	// Update is called once per frame
	void Update()
	{

	}

	private void ChangeState()
	{
		isSoundActive = !isSoundActive;
		Debug.Log("!! --> isSoundActive: " + isSoundActive);
		setIcon();
		pl.SoundsON = isSoundActive;
		PlayerPrefs.SetString("Sound", isSoundActive.ToString());
		pl.SetSound();
	}

	void setIcon() {
		Debug.Log("!! --> setIcon: " + isSoundActive);
		if (isSoundActive)
			this.GetComponent<Image>().sprite = SoundON;
		if (!isSoundActive)
			this.GetComponent<Image>().sprite = SoundOff;
	}
}
