﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BTN_Sound : MonoBehaviour {

	
	// Use this for initialization
	AudioSource audioData;

	void Start()
	{
		this.GetComponent<Button>().onClick.AddListener(PlaySound);
	}

	// Update is called once per frame
	void Update () {
		
	}

	private void PlaySound() {
		audioData = GetComponent<AudioSource>();
		audioData.Play();


	}
}

