﻿// Copyright 2014 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Collider))]
public class SelectItem : MonoBehaviour//, ICardboardGazeResponder 
{

	public int index;
	public RestClass rest;
	public bool isIn;
	public float elapsedTime;
	
	public delegate void ItemSelected(int idSelected);
	public ItemSelected itemSelected;



	//---SMB Begining
	public GameObject Reticle;
	public GameObject LoadReticle;
	public GameObject VideoController;

	//To load and play buttons on this Selecto Item Object, if is avalilable...
	public GameObject DownImage;
	public GameObject LoaddingText;
	public GameObject LoaddingCircle;
	public GameObject TrashButton;
	public GameObject TrashButton_Blank;
	public GameObject PlayButton;
	public GameObject OkButtonOff;
	public GameObject OkButtonOn;

	//SMB{
	//Textures to show if you are on or out the button, work together:
	public GameObject Sprite_On;
	public GameObject Sprite_Off;
	public GameObject Sprite_NO;
	public GameObject MasterObjectToDelete;
	//}SMB

	/*NEW*/public GameObject LoadingReticle;
	public GameObject LoadingReticle2;
	/*NEW*/public GameObject Comenzar;
	public GameObject TutorialPan;

	//---
	public GameObject deb;//for Debugs on Graphics

	//Button to Events
	public GameObject HomeButton2;

	public GameObject PlayButtonEnter;

	public GameObject PauseButton;
	public GameObject PauseButtonEnter;
	public GameObject MenuPP;

	public VideoPlayController.BUTTONS_STATE StateOfButton=VideoPlayController.BUTTONS_STATE.NONE;
	//---SMB End
	public Animation AnimRet;
	Animator AnimatorRet;


	void Start() {
		SetGazedAt(false);
		//--SMB
		VideoController = GameObject.FindGameObjectWithTag("VideoController");
		Reticle = GameObject.FindGameObjectWithTag("Reticle");
		LoadReticle = GameObject.FindGameObjectWithTag("Loading");
		HomeButton2 = GameObject.FindGameObjectWithTag("HomeBt");
		/*NEW*/LoadingReticle = GameObject.FindGameObjectWithTag("LoadingReticle");
		/*NEW*/LoadingReticle2 =GameObject.FindGameObjectWithTag("LoadingReticle2");
		AnimatorRet = LoadingReticle2.GetComponent<Animator> ();
		AnimRet = LoadingReticle2.GetComponent<Animation> ();
		AnimRet.Stop();
		if (GameObject.FindGameObjectWithTag ("Comenzar") != null) {
			Comenzar = GameObject.FindGameObjectWithTag ("Comenzar");
		}

		if (GameObject.FindGameObjectWithTag ("TutorialSplash") != null) {
			TutorialPan = GameObject.FindGameObjectWithTag ("TutorialSplash");
		}

		//Init of tools
		if(LoaddingText!=null)
			LoaddingText.SetActive (false);
		if(LoaddingCircle!=null)
			LoaddingCircle.SetActive (false);
		if(PlayButton!=null)
			PlayButton.SetActive (false);
		if(PlayButtonEnter!=null)
			PlayButtonEnter.SetActive (false);
		if ((Sprite_On != null) && (Sprite_Off != null)) {
			//On start, show ONLY the OFF position of the button sprite
			if (this.tag == "DeleteVideo") {
			} else {
				Sprite_Off.SetActive (true);
				Sprite_On.SetActive (false);
			}
		}
		if (this.tag == "Video") {
			if(VideoController!=null){
				VideoController.GetComponent<VideoPlayController>().RefrshButtons();
			}
		}
		//---SMB
	}

	public string LastAccess = "";
	public GameObject MPause;
	public GameObject MPause2;
	bool Checkin= false;
	public bool InActiveRet = false;
	void Update(){
		if (isIn) {
			elapsedTime += Time.deltaTime;
		} else {
			elapsedTime = 0;
		}
		if (elapsedTime > 3) {
			AnimatorRet.SetBool("IsActive", false);
			AnimRet.Stop();
			Debug.Log("Tag From Call: '"+this.tag+"' Obj.: '"+this.name+"'");

			switch(this.tag){

			//////////////////////////////////////////////////////////////////////////////
			//To Exit from Tutorial
			//////////////////////////////////////////////////////////////////////////////

			case "Comenzar":
				Debug.Log ("XXXXXX--COMENZAR--XXXXXXXX");
				//OnGazeExit ();
				/*NEW*/
				InActiveRet = true;

				SetGazedAt (false);
				isIn = false;
				if (Reticle != null) {
					Reticle.GetComponent<Renderer> ().enabled = true;
				}

				//TutorialPan.GetComponent<TutorialEngine> ().IsNoTutorialMode = true;
				SceneManager.LoadScene ("Categories");
				//TutorialPan.SetActive (false);
			break;


			//////////////////////////////////////////////////////////////////////////////
			//On Categories Buttons
			//////////////////////////////////////////////////////////////////////////////

			case "Categories":
				itemSelected (index);
				break;
			case "Recenter":
				Debug.Log("XXXXXXXXXXXXXX");
			//	Cardboard.SDK.Recenter();//SMB: Act to re-center the camera
				break;
			case "VRout":
				SceneManager.LoadScene ("Welcome");
				break;
			case "BackBt":
				itemSelected (index);
				break;
			case "HomeBt":
				SceneManager.LoadScene ("Categories");
				break;

			
			//////////////////////////////////////////////////////////////////////////////
			//On Video Buttons
			//////////////////////////////////////////////////////////////////////////////

			case "Video":
				if(VideoController!=null){
					VideoController.GetComponent<VideoPlayController>().Video_Press(gameObject);
				}
				break;
			case "DeleteVideo":
				Debug.Log ("DELETE VIDEO ORDER?????");
				Sprite_NO.SetActive (true);
				Sprite_Off.SetActive (false);
				Sprite_On.SetActive (false);
				isIn = false;
				if(VideoController!=null){
					VideoController.GetComponent<VideoPlayController>().DeleteVideo_Press(MasterObjectToDelete);
				}
				break;

			//////////////////////////////////////////////////////////////////////////////
			//On Play Buttons
			//////////////////////////////////////////////////////////////////////////////

			case "Pause":
				if(VideoController!=null){
					VideoController.GetComponent<VideoPlayController>().Pause_Press(gameObject);
					Sprite_Off.SetActive (true);
					Sprite_On.SetActive (false);
					isIn = false;
				}
				break;
			case "RePlay":
				if(VideoController!=null){
					VideoController.GetComponent<VideoPlayController>().RePlay_Press(gameObject);
					Sprite_Off.SetActive (true);
					Sprite_On.SetActive (false);
					isIn = false;
				}
				break;
			case "Play":
				if(VideoController!=null){
					VideoController.GetComponent<VideoPlayController>().Play_Press(gameObject);
					Sprite_Off.SetActive (true);
					Sprite_On.SetActive (false);
					isIn = false;
				}
				break;
			case "Rewind":
				if(VideoController!=null){
					VideoController.GetComponent<VideoPlayController>().Rewind_Press(gameObject);
					Sprite_Off.SetActive (true);
					Sprite_On.SetActive (false);
					isIn = false;
				}
				break;
			case "OnPlayRecenter":
				Debug.Log("XXXXXXXXXXXXXX");
				Sprite_Off.SetActive (true);
				Sprite_On.SetActive (false);
				isIn = false;
				//Cardboard.SDK.Recenter();//SMB: Act to re-center the camera
				break;
			case "BackVideos":
				if(VideoController!=null){
					//Cardboard.SDK.Recenter();//SMB: Act to re-center the camera
					Sprite_Off.SetActive (true);
					Sprite_On.SetActive (false);
					isIn = false;
					VideoController.GetComponent<VideoPlayController>().BackVideos_Press(gameObject);
				}
				break;
			case "OnPlayHome":
				SceneManager.LoadScene ("Categories");
				break;

			//////////////////////////////////////////////////////////////////////////////
			//DEFAULT
			//////////////////////////////////////////////////////////////////////////////

			default:
				itemSelected (index);
				break;
			}
		}
	}


  	void LateUpdate() {
    /*	Cardboard.SDK.UpdateState();
    	if (Cardboard.SDK.BackButtonPressed) {
      		Application.Quit();
    	}*/
  	}

	public void SetGazedAt(bool gazedAt) {
		if (GetComponent<Renderer> () != null) {
			GetComponent<Renderer> ().material.color = gazedAt ? Color.white : Color.gray;
		}
		//-------------
	}

  	#region ICardboardGazeResponder implementation


	public void OnGazeEnter() {
		bool CanYouAct = false;
		if (TutorialPan != null) {
			if (TutorialPan.activeSelf) {
				if (this.tag == "Comenzar") {
					CanYouAct = true;
				

				}
			} else {
				CanYouAct = true;
			}
		} else {
			CanYouAct = true;
		}
		if (CanYouAct){//((Comenzar == null) || (this.tag == "Comenzar")) {
			if ((Sprite_On != null) && (Sprite_Off != null)) {
				//If you are in the sprite, SHOW the ON position
				if (this.tag == "DeleteButton") {
				} else {
					Sprite_On.SetActive (true);
					Sprite_Off.SetActive (false);
				}
			}

			if (VideoController != null) {
				if ((VideoController.GetComponent<VideoPlayController> ().ActualStateController==VideoPlayController.SPHERE_STATE.PLAY_VIDEO) ||
					(VideoController.GetComponent<VideoPlayController> ().ActualStateController==VideoPlayController.SPHERE_STATE.PAUSE_VIDEO) ||
					(VideoController.GetComponent<VideoPlayController> ().ActualStateController==VideoPlayController.SPHERE_STATE.END_VIDEO) ) {
					Reticle.GetComponent<MeshRenderer> ().enabled = true;
				}
				if (this.tag == "Video") {
					VideoController.GetComponent<VideoPlayController> ().Video_OnGazeEnter (gameObject);
				}
				if (this.tag == "DeleteVideo") {
					VideoController.GetComponent<VideoPlayController> ().Delete_OnGazeEnter (MasterObjectToDelete);
				}

			}
			//if (InActiveRet = true) {
				//LoadingReticle.gameObject.GetComponent<ProgressReticle> ().LoadingRet = true;
				//LoadingReticle2.SetActive(true);
				AnimatorRet.SetBool("IsActive", true);
				//AnimRet.Rewind();
				AnimRet.Play();
				Reticle.GetComponent<Renderer> ().enabled = false;
			//} //else {
				//Reticle.GetComponent<Renderer> ().enabled = true;
				//LoadingReticle.gameObject.GetComponent<ProgressReticle> ().LoadingRet = false;
			//}
		/*-*///	Reticle.GetComponent<Renderer> ().enabled = true;
			//AnimatorRet.SetBool("IsActive", true);
			SetGazedAt (true);
			isIn = true;
			//	Reticle.gameObject.GetComponent<Renderer> ().material.color = Color.blue;
			/*NEW*//*-*///LoadingReticle.gameObject.GetComponent<ProgressReticle> ().LoadingRet = true;

			//Home Button Event On
			if (HomeButton2 != null) {
				if (HomeButton2.GetComponent<MeshRenderer> () != null) {
					HomeButton2.GetComponent<MeshRenderer> ().enabled = true;
					Debug.Log ("Button Home On");
				}
			}
			//Play Button Event On
			if (PlayButtonEnter != null) {
				PlayButtonEnter.GetComponent<MeshRenderer> ().enabled = true;
			}
			//Pause Button Event on
			if (PauseButtonEnter != null) {
				PauseButtonEnter.GetComponent<MeshRenderer> ().enabled = true;
			}
		/*NEW*/}
	}
		
  	public void OnGazeExit() {
		bool CanYouAct = false;
		if (TutorialPan != null) {
			if (TutorialPan.activeSelf) {
				if (this.tag == "Comenzar") {
					CanYouAct = true;
				}
			} else {
				CanYouAct = true;
			}
		} else {
			CanYouAct = true;
		}
		if (CanYouAct){//((Comenzar == null) || (this.tag == "Comenzar")) {
			if ((Sprite_On != null) && (Sprite_Off != null)) {
				//If you are out the sprite, SHOW the OFF position
				if (this.tag == "DeleteButton") {
				} else {
					Sprite_Off.SetActive (true);
					Sprite_On.SetActive (false);
				}
			}

			if (VideoController != null) {
				if ((VideoController.GetComponent<VideoPlayController> ().ActualStateController==VideoPlayController.SPHERE_STATE.PLAY_VIDEO) ||
					(VideoController.GetComponent<VideoPlayController> ().ActualStateController==VideoPlayController.SPHERE_STATE.PAUSE_VIDEO) ||
					(VideoController.GetComponent<VideoPlayController> ().ActualStateController==VideoPlayController.SPHERE_STATE.END_VIDEO) ) {
					Reticle.GetComponent<MeshRenderer> ().enabled = false;
				}
				if (this.tag == "Video") {
					VideoController.GetComponent<VideoPlayController> ().Video_OnGazeExit (gameObject);
				}
				if (this.tag == "DeleteVideo") {
					VideoController.GetComponent<VideoPlayController> ().Delete_OnGazeExit (MasterObjectToDelete);
				}
			}
		//	if (InActiveRet = false) {
				//LoadingReticle.gameObject.GetComponent<ProgressReticle> ().LoadingRet = false;
			//LoadingReticle2.SetActive(false);
			AnimatorRet.SetBool("IsActive", false);
			AnimRet.Stop();
			//AnimRet.Rewind ();

			if (VideoController != null) {
				if (VideoController.GetComponent<VideoPlayController> ().ActualStateController == VideoPlayController.SPHERE_STATE.PLAY_VIDEO) {
					Reticle.GetComponent<Renderer> ().enabled = false;
				} else {
					Reticle.GetComponent<Renderer> ().enabled = true;
				}
			} else {
				Reticle.GetComponent<Renderer> ().enabled = true;
			}
		

			//Reticle.GetComponent<Renderer> ().enabled = false;
		//	/*NEW*/LoadingReticle.gameObject.GetComponent<ProgressReticle> ().LoadingRet = false;
			SetGazedAt (false);
			isIn = false;
			if (Reticle != null) {
				Reticle.gameObject.GetComponent<Renderer> ().material.color = Color.white;
			}


			//Home Button Event off
			if (HomeButton2 != null) {
				if (HomeButton2.GetComponent<MeshRenderer> () != null) {
					HomeButton2.GetComponent<MeshRenderer> ().enabled = false;
					Debug.Log ("Button Home Off");
				}
			}
			//Play Button Event off
			if (PlayButtonEnter != null) {
				PlayButtonEnter.GetComponent<MeshRenderer> ().enabled = false;
			}
			//Pause Button Event off
			if (PauseButtonEnter != null) {
				PauseButtonEnter.GetComponent<MeshRenderer> ().enabled = false;
			}
		/*NEW*/}
	}

  // Called when the Cardboard trigger is used, between OnGazeEnter
  /// and OnGazeExit.
	public void OnGazeTrigger() {
		Debug.Log( "Atrapo el Click" );
  	}

  	#endregion
}

