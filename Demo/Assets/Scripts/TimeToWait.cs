﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeToWait : MonoBehaviour
{
    private float elapsedTime;
    public string SceneToGo;
    void Start()
    {
        elapsedTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;

        if (elapsedTime > 4)
        {
            SceneManager.LoadScene(SceneToGo);
        }
    }
}
