﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuDibujos : MonoBehaviour
{
    public GameObject TilePrefab;
    public int numberOfTilesNeeded;
    public GameObject Parent;
    public bool CreateTilesForTest =  true;
    public bool FavAudioCuentos = false;
    public bool FavLeer = false;
    public bool FavDibujos = false;


    void Start()
    {
        if(CreateTilesForTest)
         createAllTilesNeeded(numberOfTilesNeeded);

        if (FavAudioCuentos)
            ShowFavCuentos();
        if (FavLeer)
            ShowFavLeer();
        if (FavDibujos)
            ShowFavDibujos();
    }
    void clearAllTiles() {

        foreach (GameObject go in this.GetComponentsInChildren<GameObject>()) {
            Destroy(go);
        }
    }

    void createAllTilesNeeded(int count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject Tile = Instantiate(TilePrefab);
            Tile.transform.parent = Parent.transform;
            Tile.gameObject.name = "Tile_" + i;
        }
    }

    

    public void createTilesAndAsignAssets(int count, Sprite[] _sprites)
    {
        GameObject Parent0 = GameObject.Find("Parent0");
        GameObject Parent1 = GameObject.Find("Parent1");
        for (int i = 0; i < count; i++)
        {
            GameObject Tile = Instantiate(TilePrefab);

            if (i%2 == 0)
            Tile.transform.parent = Parent0.transform;
            else
                Tile.transform.parent = Parent1.transform;

            Tile.gameObject.name = "Tile_" + i;
            Tile.GetComponent<BTN_Personajes>().SetButtonParameters(_sprites[i], "test");
        }
    }

    public void createTilesAndAsignSpritesCEC(int count, Sprite[] _sprites)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject Tile = Instantiate(TilePrefab);
            Tile.transform.parent = Parent.transform;
            Tile.gameObject.name = "Tile_" + i;
            Tile.GetComponent<BTN_CualEsCual>().SetButtonParameters(_sprites[i], "CualEsCual_juego0" + (i + 1).ToString());
            Tile.GetComponent<AppButton>().SetScene("CualEsCual_juego0" + (i + 1).ToString());
        }
    }

    private void ShowFavCuentos() {

        PersistentLocalize persistent = GameObject.FindGameObjectWithTag("Localization").GetComponent<PersistentLocalize>();

        persistent.GetCuentosGuardados( TilePrefab, Parent);
       // Debug.Log("persistent.AudioCuentosFav.Count" + persistent.AudioCuentosFav.Count);

    }
    private void ShowFavLeer() {

        PersistentLocalize persistent = GameObject.FindGameObjectWithTag("Localization").GetComponent<PersistentLocalize>();

        persistent.GetLeerGuardados( TilePrefab, Parent);
        
    }
    private void ShowFavDibujos()
    {
        //Debug.Log("0");
        PersistentLocalize persistent = GameObject.FindGameObjectWithTag("Localization").GetComponent<PersistentLocalize>();

        persistent.GetElementsSavedInDeviceDibujos(TilePrefab, Parent);

    }
    void Update()
    {
        
    }
}
