﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class TileDibujos : MonoBehaviour
{
    public Button Compartir;
    public Button Borrar;
    public Image ImgDibujo;
    public string _urlImage;
    public byte[] IMGBytes;
    public int PlayerPrefOrderList;
    // Start is called before the first frame update
    void Start()
    {
        NativeShareScript nss = this.GetComponent<NativeShareScript>();
        nss.imgUrl = _urlImage;
        Compartir.onClick.AddListener(nss.ShareBtnPress);
        Borrar.onClick.AddListener(DeleteTileFromFavorites);
    }
    public void SetImage(string urlImage, int _order) {
        _urlImage = urlImage;
        PlayerPrefOrderList = _order;
        Texture2D t2d = LoadTextureFromFile(urlImage);
        Rect _rect = ImgDibujo.GetComponent<RectTransform>().rect;
        // ImgDibujo.sprite = Sprite.Create(t2d, new Rect(0, 0, _rect.width, _rect.height), new Vector2(0,1));//set the Rect with position and dimensions as you need
        ImgDibujo.overrideSprite = Sprite.Create(t2d, new Rect(0, 0,  t2d.width ,  t2d.height ), new Vector2(0.5f, 0.5f), 100);
    }
    public Texture2D LoadTextureFromFile(string path)
    {
        // To get image
        var bytesRead = System.IO.File.ReadAllBytes(path);
        Texture2D myTexture = new Texture2D(4, 4, TextureFormat.DXT1, false);
        //Texture2D myTexture = new Texture2D (720, 271);
        myTexture.LoadImage(bytesRead);

        return myTexture;
    }

    public void setParameters( string urlImage) {
        //   StartCoroutine(ConvertUrlToImage(urlImage));
        Debug.Log("urlImage:" + urlImage);
        //File.ReadAllBytes("file:///" + urlImage);
        var s = File.ReadAllBytes("file:///" + urlImage);
        Debug.Log("s:" + s);

        
        // var s = File.ReadAllBytes(Application.dataPath + "/" + imageFolder);
        Texture2D tex = new Texture2D(2, 2);
        tex.LoadImage(s);


    }
    
    private IEnumerator ConvertUrlToImage(string url)
    {
        
        Texture2D tex;
        tex = new Texture2D(1, 1, TextureFormat.DXT1, false);
        using (WWW www = new WWW(url))
        {
            yield return www;
            www.LoadImageIntoTexture(tex);
            Rect rec = new Rect(0, 0, tex.width, tex.height);
            Sprite sprite = Sprite.Create(tex, rec, new Vector2(0, 0), 1);
            ImgDibujo.sprite = sprite;
            IMGBytes = www.bytes;
            this.transform.localScale = new Vector3(1, 1, 1);
            
        }
    }
    
    public void DeleteTileFromFavorites()
    {
        PersistentLocalize pl = GameObject.FindGameObjectWithTag("Localization").GetComponent<PersistentLocalize>();
        pl.ImagesSavedInDevice--;
        int saved = PlayerPrefs.GetInt("Dibujos");
        Debug.Log("Saved: " + saved);        
        PlayerPrefs.SetInt("Dibujos", saved - 1);

        PlayerPrefs.DeleteKey("Dibujo" + PlayerPrefOrderList);

        File.Delete(_urlImage);
        Destroy(this.gameObject);
    }

    }
