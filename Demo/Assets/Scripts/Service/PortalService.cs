﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class PortalService : MonoBehaviour {
	//The categories
	private RestClass rest;
	public delegate void MyDelegate(ArrayList list);
	public MyDelegate myDelegate;

	//SMB{
	public string DirectoryToDownload="DownloadPortalJSON";//The Folder to storage the files download
	public string pathurl;
	public string JSONonPhone;


	public bool TestInternetConnection(){
		NetworkReachability NetReach;
		NetReach = Application.internetReachability;
		bool Result = false;
		switch (NetReach) {
		case NetworkReachability.ReachableViaLocalAreaNetwork:
			Debug.Log ("internetReachability = Can connect to Internet by LAN");
			Result = true;
			break;
		case NetworkReachability.ReachableViaCarrierDataNetwork:
			Debug.Log ("internetReachability = Can connect to Internet by CARRIER");
			Result = true;
			break;
		case NetworkReachability.NotReachable:
			Debug.Log ("internetReachability = NOT CONNECTED!!!!");
			break;
		default:
			Debug.Log ("internetReachability = ERROR!!!!");
			break;
		}
		return Result;
	}

	public string GetDirectoryPath(){
		return Application.persistentDataPath + "/" + DirectoryToDownload;
	}

	public string ConvertURLtoFileName(string URLk){//COMPLETE
		string VideoName = URLk;
		VideoName = VideoName.Replace("http://","");
		VideoName = VideoName.Replace("/","-");
		VideoName = GetDirectoryPath() + "/" + VideoName;
		return VideoName;
	}

	public bool CheckDirectory(){
		return System.IO.Directory.Exists (GetDirectoryPath());
	}

	public void CreateDirectory(){
		if (!CheckDirectory ()) {
			System.IO.Directory.CreateDirectory(GetDirectoryPath());
		}
	}

	public void DeleteDirectory(){
		if (CheckDirectory ()) {
			System.IO.Directory.Delete(GetDirectoryPath(),true);
		}
	}

	public void ClearDirectory(){
		DeleteDirectory ();
		CreateDirectory ();
	}

	public bool FindFilesFromURL(string URLx){//To check
		bool Existing = System.IO.File.Exists(ConvertURLtoFileName(URLx));
		return Existing;
	}

	public void DeleteFileFromURL(string URLz){
		if(FindFilesFromURL(URLz)){
			System.IO.File.Delete(ConvertURLtoFileName(URLz));
		}
	}

	public void SaveTextToFile(string texttosave, string pathfile){
		// to store image
		System.IO.File.WriteAllText(pathfile,texttosave);
		Debug.Log ("Saved JSON on phone!!!");
	}

	public string LoadTextFromFile(string pathfile){
		// To get image
		string myText = System.IO.File.ReadAllText (pathfile);
		return myText;
	}

	public string TryToLoadFileFromURL(string url){
		CreateDirectory ();
		if(FindFilesFromURL(url)){
			return LoadTextFromFile (ConvertURLtoFileName (url));
		}else{
			return "";
		}
	}
	public void TryToSaveJSONFileFromURL(){
		CreateDirectory ();
		if (FindFilesFromURL (pathurl)) {
			DeleteFileFromURL (pathurl);
			Debug.Log ("Pre-exist JSON on phone, delete to rewrite");
		} else {
		}
		SaveTextToFile (JSONonPhone, ConvertURLtoFileName (pathurl));
	}
	//SMB}

	public void read(){
		Debug.Log ("SMB-PortalServices: ReadFromWeb: http://udistcms.com/api/portals/59");//SMB: No call....
		pathurl="http://udistcms.com/api/portals/59";

		JSONonPhone=TryToLoadFileFromURL(pathurl);
		Debug.Log ("JSON on phone: "+JSONonPhone);
		//SMB: frist, test internet connection...
		if (TestInternetConnection ()) {
			rest = gameObject.AddComponent<RestClass> ();
			rest.GET ("http://udistcms.com/api/portals/59", onComplete);//JSon Web File
		} else {
			if (JSONonPhone != "") {
				onCompleteDirectFromFile ();
			}
		}
	}

	public void onComplete(string r){
		if (r != JSONonPhone) {
			JSONonPhone = r;
			Debug.Log ("Write JSON on phone: "+JSONonPhone);
			TryToSaveJSONFileFromURL ();
		}

		//Debug.Log ("R: " + r);//SMB: Debug anulado
		var response = JSON.Parse(r);
		//Debug.Log ("response: " + response.Count);//SMB: Debug anulado
		var categories = response["Categories"];
		//Debug.Log ("categories: " + categories.Count);//SMB: Debug anulado

		ArrayList result = new ArrayList ();

		for (int i=0; i<categories.Count; i++ ){

			var category = categories [i];
			Category cat = new Category ();

			cat.id = category ["Id"].AsInt;
			cat.name = category ["Name"].Value;
			cat.url = category ["Url"].Value;
			cat.BgUrl2 = category ["Url2"].Value;
			//---smb

			if(cat.id != 277){
			//---smb
			result.Add (cat);
			}
		}

		myDelegate (result);
	}

	//SMB{
	public void onCompleteDirectFromFile(){
		var response = JSON.Parse(JSONonPhone);
		//Debug.Log ("response: " + response.Count);//SMB: Debug anulado
		var categories = response["Categories"];
		//Debug.Log ("categories: " + categories.Count);//SMB: Debug anulado

		ArrayList result = new ArrayList ();

		for (int i=0; i<categories.Count; i++ ){

			var category = categories [i];
			Category cat = new Category ();

			cat.id = category ["Id"].AsInt;
			cat.name = category ["Name"].Value;
			cat.url = category ["Url"].Value;
			cat.BgUrl2 = category ["Url2"].Value;
			//---smb

			if(cat.id != 277){
				//---smb
				result.Add (cat);
			}
		}

		myDelegate (result);
	}
	//SMB}
}

public class Category {
	public int id;
	public string name;
	public string url;
	public string BgUrl2;



}


//http://udistcms.com/api/portals/72

//{"Id":72,
//"Name":"Alicia (JC)",
//"Title":"Alicia ¿Qué hay del otro lado?",
//"Country":{"Id":1,"Name":"Argentina","Code":"AR"},
//"CountryId":1,
//"Application":null,
//"ApplicationId":null,
//"ThumbnailSmallUrl":"http://udistcms.com/cdn/portal/72/small_thumb.jpg",
//"ThumbnailMediumUrl":"http://udistcms.com/cdn/portal/72/medium_thumb.jpg",
//"ThumbnailLargeUrl":"http://udistcms.com/cdn/portal/72/large_thumb.jpg",
//"Categories":
	//[{"Id":254,"Name":"Videos 360","Description":null,"ParentId":null,"InMenu":true,"Order":0,"PortalId":72,"UrlExtension":".jpg","Url":"http://udistcms.com/cdn/portal/72/category/254/file.jpg","PortalContents":null}
	//,{"Id":253,"Name":"Videos","Description":null,"ParentId":null,"InMenu":true,"Order":1,"PortalId":72,"UrlExtension":".jpg","Url":"http://udistcms.com/cdn/portal/72/category/253/file.jpg","PortalContents":null},
	//{"Id":255,"Name":"Fotos","Description":null,"ParentId":null,"InMenu":true,"Order":2,"PortalId":72,"UrlExtension":".jpg","Url":"http://udistcms.com/cdn/portal/72/category/255/file.jpg","PortalContents":null},
	//{"Id":278,"Name":"La obra","Description":null,"ParentId":null,"InMenu":false,"Order":3,"PortalId":72,"UrlExtension":".jpg","Url":"http://udistcms.com/cdn/portal/72/category/278/file.jpg","PortalContents":null}],
//"Operators":[{"Id":1,"Name":"Personal","CountryId":1,"Country":{"Id":1,"Name":"Argentina","Code":"AR"}}],"Disclaimer":null,"Aggregator":{"Id":1,"Name":"Unlimited Distribution"},"AggregatorId":1,"CreationDate":"2016-06-08T10:42:32.367","Url":null,"TrackingId":null,"Active":true}