﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;


public class AudioCuentosTiles : MonoBehaviour
{
    public bool iSFavoriteScene = false;
    [Header("Tile Parameters")]
    public Image background;
    public string VideoName;
    public string urlToOpen;
    public string imagePath;


    [Header("Tile Favorites Parameters")]
    public bool isFavoriteTile = false;
    public Image DowloadedVideoImage;
    public Color32 NotDownloaded;
    public Button DeleteFavVideo;


    [Header("Json Parameters")]
    public int Id;
    public int ContentId;
    public string TypeName;
    public string CategoryName;
    public string Url;
    public string ThumbnailOriginallUrl;
    public string ThumbnailSmallUrl;
    public string ThumbnailMediumUrl;
    public string ThumbnailLargeUrl;
    public string Title;
    [Header("Aux Parameters")]
    public bool isVideoDowloaded;
    public byte[] IMGBytes;
    public string UrlOfSavedImage;
    [Header("PlayerPrefs Parameters")]
    public string SavedImageInDevice;
    public string TypeTile;


    [Header("Tools")]
    public GameObject WindowVideoReproducer;

    void Start()
    {
        WindowVideoReproducer = GameObject.FindWithTag("Window_VideoPlayer");
        // Debug.Log("iSFavoriteScene: " + iSFavoriteScene);
        Button btn = this.GetComponent<Button>();
        if (!iSFavoriteScene)
        {
            btn.onClick.AddListener(_OpenVideoReproducer);
        }
        else {

            btn.onClick.AddListener(_OpenVideoReproducerFav);
            DowloadedVideoImage = this.GetComponent<Image>();
        }

    }


    public void SetParameters(int id, string _img, string _name, string _urlToOpen) {
        StartCoroutine(ConvertUrlToImage(_img));

        Id = id;
        VideoName = _name;
        urlToOpen = _urlToOpen;
        imagePath = _img;

    }
    public void SetParametersFav(int id, string _img, string _name, string _urlToOpen, string _Type, int _order)
    {
        Debug.Log("_img: " + _img);
        Debug.Log("_urlToOpen: " + _urlToOpen);
        Id = id;
        VideoName = _name;
        urlToOpen = _urlToOpen;
        imagePath = _img;
        Cuento = _Type;
        SetImage(Id);
        PlayerPrefOrderList = _order;
    }
    public string Cuento = "";//"Audio","Leer"
    public void SetImage(int _id_)
    {
        string ImgID = "";
        Debug.Log("IdIdId: " + _id_);
        if (Cuento == "Audio")
        {
            ImgID = PlayerPrefs.GetString("AudioImage" + _id_);
        }
        if (Cuento == "Leer")
        {
            ImgID = PlayerPrefs.GetString("LeerImage" + _id_);
        }
        if (ImgID != null)
        {
            string ImageURL = ImgID;
            //  Debug.Log("ImageURL: " + ImageURL);
            Texture2D t2d = LoadTextureFromFile(ImageURL);
            //Rect _rect = DowloadedVideoImage.transform.GetComponent<RectTransform>().rect;
            //DowloadedVideoImage.sprite = Sprite.Create(t2d, new Rect(0, 0, _rect.width, _rect.height), new Vector2());//set the Rect with position and dimensions as you need
            Sprite mySprite = Sprite.Create(t2d, new Rect(0.0f, 0.0f, t2d.width, t2d.height), new Vector2());
            //Debug.Log("DowloadedVideoImage: " + DowloadedVideoImage.name);
            DowloadedVideoImage.sprite = mySprite;
        }
    }
    public Texture2D LoadTextureFromFile(string path)
    {
        // To get image
        var bytesRead = System.IO.File.ReadAllBytes(path);
        Texture2D myTexture = new Texture2D(4, 4, TextureFormat.DXT1, false);
        //Texture2D myTexture = new Texture2D (720, 271);
        myTexture.LoadImage(bytesRead);

        return myTexture;
    }

    private void _OpenVideoReproducer() {
        //  Debug.Log("Click AudioCuentos button");
        WindowVideoReproducer.GetComponent<WindowVideoPlayer>().OpenWindow(Url);
        MediaPlayerCtrl_Mod MPCM = WindowVideoReproducer.GetComponent<WindowVideoPlayer>().videoRep.GetComponent<MediaPlayerCtrl_Mod>();
        MPCM.DownloadStreamingVideoAndLoad(Url, Id.ToString(), this.GetComponent<AudioCuentosTiles>());
        MPCM.GetComponent<MediaPlayerCtrl_Mod>().Play();
        WindowVideoReproducer.GetComponent<WindowVideoPlayer>().SelectedTile = this.GetComponent<AudioCuentosTiles>();
        WindowVideoReproducer.GetComponent<WindowVideoPlayer>().SelectedTileName = Title;


    }
    private void _OpenVideoReproducerFav()
    {
        Debug.Log("Click AudioCuentos button");
        WindowVideoPlayer wvp = GameObject.FindWithTag("Window_VideoPlayer").GetComponent<WindowVideoPlayer>();
        wvp.DebugText.text = urlToOpen;
        ///Load("file://" + write_path);
        ///
        WindowVideoReproducer.GetComponent<WindowVideoPlayer>().OpenWindow("file://" + urlToOpen);
        /*
        #if UNITY_EDITOR
                WindowVideoReproducer.GetComponent<WindowVideoPlayer>().OpenWindow(urlToOpen);
        #endif

        #if __ANDROID__
                WindowVideoReproducer.GetComponent<WindowVideoPlayer>().OpenWindow("file://"+urlToOpen);
        #endif*/
        //WindowVideoReproducer.GetComponent<WindowVideoPlayer>().OpenWindow(Application.persistentDataPath + "files/" + "Data/"+Id+"file.mp4");
        WindowVideoReproducer.GetComponent<WindowVideoPlayer>().SelectedTile = this.GetComponent<AudioCuentosTiles>();
        WindowVideoReproducer.GetComponent<WindowVideoPlayer>().SelectedTileName = Title;


    }

    private IEnumerator ConvertUrlToImage(string url)
    {
        //Debug.Log("url: "+ url);
        Texture2D tex;
        tex = new Texture2D(1, 1, TextureFormat.DXT1, false);
        using (WWW www = new WWW(url))
        {
            yield return www;
            www.LoadImageIntoTexture(tex);
            Rect rec = new Rect(0, 0, tex.width, tex.height);
            Sprite sprite = Sprite.Create(tex, rec, new Vector2(0, 0), 1);
            background.sprite = sprite;
            IMGBytes = www.bytes;

        }
    }
    public int PlayerPrefOrderList = 0;
    public bool isVideoDownloaded = false;
    public void DeleteTileFromFavorites() {
        PersistentLocalize pl = GameObject.FindGameObjectWithTag("Localization").GetComponent<PersistentLocalize>();
        if (Cuento == "Audio")
        {
            PlayerPrefs.DeleteKey("AudioID" + PlayerPrefOrderList);
            PlayerPrefs.DeleteKey("AudioType" + ContentId);
            PlayerPrefs.DeleteKey("AudioTitle" + ContentId);
            PlayerPrefs.DeleteKey("AudioImage" + ContentId);
            PlayerPrefs.DeleteKey("AudioImage" + ContentId);
          
            if (isVideoDownloaded == true)
            {
                File.Delete(urlToOpen);
            }
                File.Delete(imagePath);

            //   _urlVideo = Application.persistentDataPath + "/" + "Data/" + _id + "file.mp4";
            Destroy(this.gameObject);
            pl.AuxElements--;
            int saved = PlayerPrefs.GetInt("AudioCuentosGuardados");
            Debug.Log("Saved: " + saved);
            PlayerPrefs.SetInt("AudioCuentosGuardados", saved - 1);
        }
        if (Cuento == "Leer")
        {
            PlayerPrefs.DeleteKey("LeerID" + PlayerPrefOrderList);
            PlayerPrefs.DeleteKey("LeerType" + PlayerPrefOrderList);
            PlayerPrefs.DeleteKey("LeerTitle" + PlayerPrefOrderList);
            PlayerPrefs.DeleteKey("LeerImage" + ContentId);
            PlayerPrefs.DeleteKey("LeerImage" + ContentId);
         
            if (isVideoDownloaded == true)
            {
                File.Delete(urlToOpen);
            }
            File.Delete(imagePath);
            
            Destroy(this.gameObject);
            pl.AuxElementsLeer--;
            int saved = PlayerPrefs.GetInt("LeerCuentosGuardados");
            Debug.Log("Saved: " + saved);
            PlayerPrefs.SetInt("LeerCuentosGuardados", saved - 1);
            //   _urlVideo = Application.persistentDataPath + "/" + "Data/" + _id + "file.mp4";

        }
    }
    
}
