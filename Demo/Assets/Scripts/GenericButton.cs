﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenericButton : MonoBehaviour
{
    public GameObject MenuToOpen;
    private FavoritosSecciones fav;
    public Text TextoBTN;
   
    void Start()
    {
        fav = GameObject.FindWithTag("BotonesFavoritos").GetComponent<FavoritosSecciones>();
        Button btn = this.GetComponent<Button>();
        btn.onClick.AddListener(GoToCorrectMenu);
    }

   
    void Update()
    {
        
    }

    void GoToCorrectMenu()
    {
        fav.closeOtherMenus();
        MenuToOpen.SetActive(true);
        this.GetComponent<Image>().color = fav.StateColors[1];
        TextoBTN.GetComponent<Text>().color = fav.StateColors[0];
    }

    public void closeOtherMenus()
    {
        
        gameObject.GetComponent<Image>().color = fav.StateColors[0];
        TextoBTN.GetComponent<Text>().color = fav.StateColors[1];

    }

}
