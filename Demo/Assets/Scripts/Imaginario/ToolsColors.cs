﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using unitycoder_MobilePaint;

public class ToolsColors : MonoBehaviour
{
    [Header("Prefab")]
    public BTN_Color BTNColorPrefab;
    [Header("Parent")]
    public GameObject Parent;
    [Header("Colors")]
    public int CountOfButtons;
    public Color32[] Colors;
    public Button[] ColorsBtns;
    [Header("External Elements")]
    public ColorUIManager cm;

    void Awake()
    {

    }

    void Start()
    {
       // createAllColorsNeeded(CountOfButtons);
    }


    void Update()
    {

    }
    void createAllColorsNeeded(int count)
    {
        ColorsBtns = new Button[count];

        for (int i = 0; i < count; i++)
        {
            GameObject button = Instantiate(BTNColorPrefab.gameObject);
            button.transform.parent = Parent.transform;
            button.gameObject.name = "BTN_Color_" + i;
            button.GetComponent<BTN_Color>().SetButtonParameters(Colors[i]);
            Button b = button.GetComponent<Button>();
            ColorsBtns[i] = b;
        }

       // cm.setColorPickers(ColorsBtns);
        //cm.SetColorsInButtons();
    }
}
