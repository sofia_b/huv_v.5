﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tools_Panel : MonoBehaviour
{
    [Header("Botones")]
    public Button Borrar;
    public Button Crayon;
    public Button Lapiz;
    public Button Pincel;
    public Button Deshacer;

    /*[Header("Colores")]
    public Color ActiveColor;
    public Color NormalColor;*/


    // Start is called before the first frame update
    void Start()
    {
        Button btn = this.GetComponent<Button>();
        btn.onClick.AddListener(activeJustThisButton);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void activeJustThisButton() {

        Borrar.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        Crayon.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        Lapiz.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        Pincel.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        Deshacer.GetComponent<Image>().color = new Color32(255, 255, 255, 255);

        this.GetComponent<Image>().color = new Color32(155, 155, 155, 255);
    }
}
