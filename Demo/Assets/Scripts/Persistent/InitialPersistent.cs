﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InitialPersistent : MonoBehaviour {

	public Button Lang_ES;
	public Button Lang_PT;
	public Button Lang_EN;
	public GameObject BlackPanel;
	public string SettedLanguage;

	public bool isFirstTime = true;
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(transform.gameObject);
		if (PlayerPrefs.GetString("IsFirstTime") != null) {
			string first =  PlayerPrefs.GetString("IsFirstTime");
			if (first == "false")
				isFirstTime = false;
		}

		if (isFirstTime == true)
		{
			BlackPanel.SetActive(false);


		}
		else {
			SceneManager.LoadScene(1);
		
		
		}

		
	
		Lang_ES.onClick.AddListener(SetLangES);
		Lang_PT.onClick.AddListener(SetLangPT);
		Lang_EN.onClick.AddListener(SetLangEN);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void SetLangES() {
		SettedLanguage = "Spanish";
		StartCoroutine(gotosceneSecond());
	}

	IEnumerator gotosceneSecond()
	{
		yield return new WaitForSeconds(0.5f);
		SceneManager.LoadScene(1);
	}

		private void SetLangPT()
	{
		SettedLanguage = "Portuguese";
		StartCoroutine(gotosceneSecond());
	}
	private void SetLangEN()
	{
		SettedLanguage = "English";
		StartCoroutine(gotosceneSecond());
	}
}
