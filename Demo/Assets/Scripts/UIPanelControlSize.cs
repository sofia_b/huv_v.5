﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPanelControlSize : MonoBehaviour
{
    public GameObject ParentScrollView;
    
    /*
        rectTransform.offsetMin = new Vector2(rectTransform.offsetMin.x, bottom);
        rectTransform.offsetMax = new Vector2(rectTransform.offsetMax.x, top);

         */
    public void expandHeightOfPanel(float tam) {
        float ThisSize = ParentScrollView.GetComponent<RectTransform>().rect.height;
        Debug.Log("ThisSize.y: " + ThisSize);

        RectTransform ThisTrans = this.GetComponent<RectTransform>();
        ThisTrans.offsetMin = new Vector2(ThisTrans.offsetMin.x, -tam);// + ThisSize);
        Debug.Log("ThisTrans.offsetMin.y: " + ThisTrans.offsetMin.y);
    }
    

}
