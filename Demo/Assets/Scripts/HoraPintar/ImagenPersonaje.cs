﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using unitycoder_MobilePaint;

public class ImagenPersonaje : MonoBehaviour
{
    public Texture[] SelectedCharacter;
    public GameObject[] SelectedCharacterCanvas;
    public ColorUIManager CUIM;
    public GameObject PersistentObject;
    private bool Verify = true;
    public PaintManager _PaintManager;
    // Start is called before the first frame update
    void Start()
    {
        PersistentObject = GameObject.FindGameObjectWithTag("Localization");
        SelectTexture();
    }

    void Update() {
       /* if (Verify == true)
        {
            if (PersistentObject != null)
            {
                SelectTexture();
            }
        }*/
    
    }
    private IEnumerator SetCharacter() {
        yield return new WaitForSeconds(1);
        


    }
    void SelectTexture() {
      //  Debug.Log("Character??: " + PersistentObject.GetComponent<PersistentLocalize>().CharacterSelected);
    
        for (int i = 0; i < SelectedCharacterCanvas.Length; i++) {
            //Debug.Log("SelectedCharacter"+i+"??: " + SelectedCharacter[i].name);
            string Name = "DrawingPlaneCanvas" + PersistentObject.GetComponent<PersistentLocalize>().CharacterSelected;
            if (SelectedCharacterCanvas[i].name == Name)
            {
            //    Debug.Log("Esteeeee!!!!: " + SelectedCharacterCanvas[i].name + ", " + Name);
                SelectedCharacterCanvas[i].SetActive(true);
                _PaintManager.mobilePaintReference = SelectedCharacterCanvas[i].GetComponent<MobilePaint>();
                CUIM.setMobile();
            }
            else{ SelectedCharacterCanvas[i].SetActive(false); }
        }

        

        Verify = false;
    }
}
