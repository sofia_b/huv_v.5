﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BTN_Personajes : MonoBehaviour
{
    public Image ButtonImage;
    public Sprite ImageToInsert;
    public string DrawToOpen;
    PersistentLocalize pl;
   

   
    void Start()
    {
      //  SetButtonParameters(ImageToInsert,"testeo");
        Button btn = this.GetComponent<Button>();
        btn.onClick.AddListener(OpenCharacterSelected);
        pl = GameObject.FindWithTag("Localization").GetComponent<PersistentLocalize>();
    }

    public void SetButtonParameters(Sprite image, string urlToGo) {
        ButtonImage.sprite = image;
        DrawToOpen = urlToGo;
    }

    private void OpenCharacterSelected() {
      //  Debug.Log("Click Personajes button");
        pl.CharacterSelected = DrawToOpen;
        SceneManager.LoadScene("SCN_HoraDePintar_Paint"); 
    }
    void Update()
    {
        
    }
}
